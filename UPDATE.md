LibertyCode_Di
==============



Version "1.0.0"
---------------

- Create repository

- Set dependency

- Set builder

- Set provider

---



Version "1.0.1"
---------------

- Update dependency

    - Set register as external library
    - Set dependency factory
    
- Update builder
    
    - Use dependency factory

- Set factory

---


