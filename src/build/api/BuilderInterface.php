<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified dependency collection instance, with dependencies.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\build\api;

use liberty_code\di\dependency\api\DependencyCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified dependency collection.
     *
     * @param DependencyCollectionInterface $objDependencyCollection
     * @param boolean $boolClear = true
     */
    public function hydrateDependencyCollection(DependencyCollectionInterface $objDependencyCollection, $boolClear = true);
}