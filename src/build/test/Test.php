<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/dependency/factory/test/DependencyFactoryTest.php');

// Use
use liberty_code\data\data\table\model\TableData;
use liberty_code\register\item\instance\model\InstanceCollection;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\config\model\DefaultConfig;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\build\model\DefaultBuilder;



// Init var
$objConfig = DefaultConfig::instanceGetDefault();

$objData = new TableData();
$objConfigExt = new DataConfig($objData);
$objConfigExt->getObjData()->setDataSrc(array(
    'conf_key_1' => 'Value config 1',
    'conf_key_2' => 2.77,
    'conf_key_3' => true,
));

$objRegister = new MemoryRegister(new InstanceCollection());

$objDepCollection = new DefaultDependencyCollection($objRegister, $objConfigExt);
$objBuilder = new DefaultBuilder($objDependencyFactory);

$tabDataSrc = array(
    'liberty_code\\di\\dependency\\test\\ClassTest2' => [
        'set' => 'liberty_code\\di\\dependency\\test\\ClassTest6'
    ],
    'liberty_code\\di\\dependency\\test\\ClassTest7' => [
        'set' =>  ['type' => 'dependency', 'value' => 'svc_4']
    ],
    'svc_1' => [
		'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
		'argument' => [
			['type' => 'numeric', 'value' => 1.7],
			['type' => 'string', 'value' => 'service 1'],
			['type' => 'boolean', 'value' => 1],
			['type' => 'array', 'value' => [
					['key' => 'key_1', 'type' => 'string', 'value' => 'service 1 array'],
					['key' => 'key_2', 'type' => 'numeric', 'value' => 3],
					['type' => 'config', 'value' => 'conf_key_3'],
					['key' => 'key_4', 'type' => 'array', 'value' => [
							['key' => 'key_4_1', 'type' => 'string', 'value' => 'service 1 array array'],
							['key' => 'key_4_2', 'type' => 'numeric', 'value' => '7'],
							['key' => 'key_4_3', 'type' => 'boolean', 'value' => false]
						]
					]
				]
			],
		],
		'option' => [
			'shared' => true
		]
	],
    'svc_2' => [
		'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
		'argument' => [
			['type' => 'config', 'value' => 'conf_key_2'],
			['type' => 'string', 'value' => 'service 2'],
            true //,
			//['type' => 'array', 'value' => []] // Test argument with default value
		]
	],
    'svc_3' => [
		'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
		'argument' => [
			['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest5'],
			['type' => 'callback_return', 'value' => 'new \\DateTime()'],
			['type' => 'callback_return', 'value' => '9'] //,
			//['type' => 'array', 'value' => []] // Test argument with default value
		]
	],
    'svc_4' => [
		'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
		'argument' => [
			['type' => 'dependency', 'value' => 'svc_1'],
			['type' => 'mixed', 'value' => new \DateTime()],
			['type' => 'boolean', 'value' => '9'] //,
			//['type' => 'array', 'value' => []] // Test argument with default value
		],
		'option' => [
			'shared' => 1
		]
	],
    'svc_5' => [
		'source' => 'liberty_code\\di\\dependency\\test\\ClassTest4',
		'argument' => [
			['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest3']
			//['type' => 'array', 'value' => []] // Test argument with default value
		],
		'option' => [
			//'shared' => 1
		]
	]
);

// Test configuration
//$objConfig->setBoolAutoConfig(true);
//$objConfig->setIntMaxLimitRecursion(75); // Max 159 before fatal error



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate dependency collection
echo('Test hydrate dependency collection: <br />');

$objBuilder->hydrateDependencyCollection($objDepCollection);
foreach($objDepCollection as $strKey => $objDep)
{
	try{
		echo('Dependency "' . $strKey . '":');echo('<br />');
		echo('Get: key: <pre>');print_r($objDep->getStrKey());echo('</pre>');
		echo('Get: class path: <pre>');print_r($objDep->getStrClassPath());echo('</pre>');
		echo('Get: config: <pre>');print_r($objDep->getTabConfig());echo('</pre>');
		//echo('Get: instance: <pre>');print_r($objDep->getObjInstance());echo('</pre>');
		echo('Get: dependency collection count: <pre>');print_r(count($objDep->getObjDependencyCollection()));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');


