<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\factory\library;



class ConstFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PROVIDER = 'objProvider';


	
    // Exception message constants
    const EXCEPT_MSG_PROVIDER_INVALID_FORMAT = 'Following DI provider "%1$s" invalid! It must be a provider object.';
}