<?php
/**
 * Description :
 * This class allows to define DI default factory class.
 * DI default factory allows to provide new instance,
 * from specified DI provider.
 * Can be consider is base of all factory types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\factory\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\di\factory\library\ConstFactory;
use liberty_code\di\factory\exception\ProviderInvalidFormatException;



/**
 * @method null|ProviderInterface getObjProvider() Get DI provider object.
 * @method void setObjProvider(null|ProviderInterface $objProvider) Set DI provider object.
 */
abstract class DefaultFactory extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider = null
     */
    public function __construct(ProviderInterface $objProvider = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init DI provider
        $this->setObjProvider($objProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFactory::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstFactory::DATA_KEY_DEFAULT_PROVIDER] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFactory::DATA_KEY_DEFAULT_PROVIDER
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFactory::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get new object instance,
     * from specified string class path.
     *
     * @param string $strClassPath
     * @return null|mixed
     */
    protected function getObjInstance($strClassPath)
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();

        // Get new instance, if required
        if(!is_null($objProvider))
        {
            $result = $objProvider->getFromClassPath($strClassPath);
        }

        // Return result
        return $result;
    }



}