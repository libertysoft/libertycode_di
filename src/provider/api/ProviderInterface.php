<?php
/**
 * Description :
 * This class allows to describe behavior of provider class.
 * Provider is main interface allows to get configured instances.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\provider\api;

use liberty_code\di\dependency\api\DependencyCollectionInterface;



interface ProviderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if instance object exists from specified key.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkKeyExists($strKey);
	
	
	
	/**
	 * Check if instance object exists from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return boolean
	 */
	public function checkClassPathExists($strClassPath);
	
	
	
	/**
	 * Check if instance object exists from specified identifier.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get dependency collection object.
	 *
	 * @return null|DependencyCollectionInterface
	 */
	public function getObjDependencyCollection();
	
	
	
	/**
	 * Get instance object from specified key.
	 * 
	 * @param string $strKey
	 * @return null|mixed
	 */
	public function getFromKey($strKey);
	
	
	
	/**
	 * Get instance object from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return null|mixed
	 */
	public function getFromClassPath($strClassPath);
	
	
	
	/**
	 * Get instance object from specified identifier.
	 * 
	 * @param string $strKey
	 * @return null|mixed
	 */
	public function get($strKey);



    /**
     * Get callback function,
     * to execute specified function configuration, and return result, if required.
     * It can use specified optional array of arguments, if provided.
     *
     * Function configuration format:
     * @see DependencyCollectionInterface::getCallableFunction() .
     *
     * @param mixed $configFunction
     * @param array $tabArg = array()
     * @return null|callable
     */
    public function getCallable($configFunction, array $tabArg = array());




	
	// Methods setters
	// ******************************************************************************

	/**
	 * Set dependency collection object.
	 * 
	 * @param DependencyCollectionInterface $objDependencyCollection
	 */
	public function setDependencyCollection(DependencyCollectionInterface $objDependencyCollection);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified function configuration,
     * and return result, if required.
     * It can use specified optional array of arguments, if provided.
     *
     * Function configuration format:
     * @see ProviderInterface::getCallable() .
     *
     * @param mixed $configFunction
     * @param array $tabArg = array()
     * @return mixed
     */
    public function call($configFunction, array $tabArg = array());
}