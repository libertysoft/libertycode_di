<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\provider\library;



class ConstProvider
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION = 'objDependencyCollection';

    // Exception message constants
    const EXCEPT_MSG_DEPENDENCY_COLLECTION_INVALID_FORMAT = 'Following dependency collection "%1$s" invalid! It must be a dependency collection object.';
}