<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/dependency/factory/test/DependencyFactoryTest.php');

// Load test
require_once($strRootAppPath . '/src/dependency/test/InterfaceTest1.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest1.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest2.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest3.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest4.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest5.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest6.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest7.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest8.php');

// Use
use liberty_code\data\data\table\model\TableData;
use liberty_code\register\item\instance\model\InstanceCollection;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\config\model\DefaultConfig;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\build\model\DefaultBuilder;
use liberty_code\di\provider\model\DefaultProvider;





// Init var
$objConfig = DefaultConfig::instanceGetDefault();

$objData = new TableData();
$objConfigExt = new DataConfig($objData);
$objConfigExt->getObjData()->setDataSrc(array(
    'conf_key_1' => 'Value config 1',
    'conf_key_2' => 2.77,
    'conf_key_3' => true,
));

$objRegister = new MemoryRegister(new InstanceCollection());

$tabConfig = array(
    //'search_cache_only_require' => true,
    'select_class_path' => 'last',
    'force_access_require' => 1,
    //'auto_config_require' => 1,
    //'max_limit_recursion' => 3
);

$objDepCollection = new DefaultDependencyCollection($objRegister, $objConfigExt, $tabConfig);
$objBuilder = new DefaultBuilder($objDependencyFactory);

$tabDataSrc = array(
    'svc_1' => [
        'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
        'argument' => [
            ['type' => 'numeric', 'value' => 1.7],
            ['type' => 'string', 'value' => 'service 1'],
            ['type' => 'boolean', 'value' => 1],
            ['type' => 'array', 'value' => [
                ['key' => 'key_1', 'type' => 'string', 'value' => 'service 1 array'],
                ['key' => 'key_2', 'type' => 'numeric', 'value' => 3],
                ['type' => 'config', 'value' => 'conf_key_3'],
                ['key' => 'key_4', 'type' => 'array', 'value' => [
                    ['key' => 'key_4_1', 'type' => 'string', 'value' => 'service 1 array array'],
                    ['key' => 'key_4_2', 'type' => 'numeric', 'value' => '7'],
                    ['key' => 'key_4_3', 'type' => 'boolean', 'value' => false]
                ]
                ]
            ]
            ],
        ],
        'option' => [
            'shared' => true,
			//'class_wired' => 0
        ]
    ],
    'svc_4' => [
        'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
        'argument' => [
            ['type' => 'dependency', 'value' => 'svc_1'],
            ['type' => 'mixed', 'value' => new \DateTime()],
            ['type' => 'boolean', 'value' => '9'] //,
            //['type' => 'array', 'value' => []] // Test argument with default value
        ],
        'option' => [
            'shared' => 1
        ]
    ],
    'svc_5' => [
        'source' => 'liberty_code\\di\\dependency\\test\\ClassTest4',
        'argument' => [
            ['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest3']
            //['type' => 'array', 'value' => []] // Test argument with default value
        ],
        'option' => [
            //'shared' => 1
        ]
    ],
    'liberty_code\\di\\dependency\\test\\ClassTest2' => [
        'set' => 'liberty_code\\di\\dependency\\test\\ClassTest6'
    ],
    'liberty_code\\di\\dependency\\test\\ClassTest7' => [
        'set' =>  ['type' => 'dependency', 'value' => 'svc_4']
    ],
    'liberty_code\\di\\dependency\\test\\InterfaceTest1' => [
        'key' => 'pref_3',
        'set' =>  'liberty_code\\di\\dependency\\test\\ClassTest4'
    ]
);

// Test configuration
//$objConfig->setBoolAutoConfig(true);
//$objConfig->setIntMaxLimitRecursion(75); // Max 159 before fatal error



// Hydrate dependency collection
$objBuilder->setTabDataSrc($tabDataSrc);
$objBuilder->hydrateDependencyCollection($objDepCollection);



// Build provider
$objDefaultProvider = new DefaultProvider($objDepCollection);



// Test check, get
$tabKey = array(
	'svc_1', // Found
	'svc_4', // Found
	'svc_5', // Found
    'pref_3', // Found, get class 4
	'liberty_code\\di\\dependency\\test\\ClassTest1', // Found
	'liberty_code\\di\\dependency\\test\\ClassTest2', // Found
	'liberty_code\\di\\dependency\\test\\ClassTest3', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest4', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest5', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest6', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest7', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest8', // Found
	'liberty_code\\di\\dependency\\test\\ClassTest9', // Not found
    'liberty_code\\di\\dependency\\test\\InterfaceTest1', // Found, get class 4
	'test', // Not found
	'/', // Not found
	'', // Bad format
	5 // Found
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "'.$strKey.'": <br />');
	try{
		echo('Check: <pre>');var_dump($objDefaultProvider->checkExists($strKey));echo('</pre>');
		echo('Get: <pre>');print_r($objDefaultProvider->get($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');


