<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\provider\exception;

use liberty_code\di\provider\library\ConstProvider;
use liberty_code\di\dependency\api\DependencyCollectionInterface;



class DependencyCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $dependencyCollection
     */
	public function __construct($dependencyCollection) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstProvider::EXCEPT_MSG_DEPENDENCY_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($dependencyCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified dependency collection has valid format
	 * 
     * @param mixed $dependencyCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($dependencyCollection)
    {
		// Init var
		$result = (
			(is_null($dependencyCollection)) || 
			($dependencyCollection instanceof DependencyCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($dependencyCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}