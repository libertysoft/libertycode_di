<?php
/**
 * Description :
 * This class allows to define default provider class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\provider\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\di\provider\api\ProviderInterface;

use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\di\provider\library\ConstProvider;
use liberty_code\di\provider\exception\DependencyCollectionInvalidFormatException;



class DefaultProvider extends FixBean implements ProviderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * Constructor
	 * 
	 * @param DependencyCollectionInterface $objDependencyCollection = null
     */
	public function __construct(DependencyCollectionInterface $objDependencyCollection = null) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Hydrate dependency collection if needs
		if(!is_null($objDependencyCollection))
		{
			$this->setDependencyCollection($objDependencyCollection);
		}
	}
	
	
	
	
	
	// Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION))
        {
            $this->beanAdd(ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION, null);
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION:
					DependencyCollectionInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





	// Methods value
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkKeyExists($strKey)
	{
		// Return result
		return (!is_null($this->getFromKey($strKey)));
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function checkClassPathExists($strClassPath)
	{
		// Return result
		return (!is_null($this->getFromClassPath($strClassPath)));
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function checkExists($strKey)
	{
		// Return result
		return (!is_null($this->get($strKey)));
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function getObjDependencyCollection()
	{
		// Return result
        return $this->beanGet(ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION);
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getFromKey($strKey)
	{
		// Init var
		$result = null;
		$objDepCollection = $this->getObjDependencyCollection();
		$objDep = $objDepCollection->getObjDependency($strKey);
		
		// Get instance if dependency found
		if(!is_null($objDep))
		{
			$result = $objDep->getObjInstance();
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function getFromClassPath($strClassPath)
	{
		// Init var
		$objDepCollection = $this->getObjDependencyCollection();
		$result = $objDepCollection->getObjInstance($strClassPath);
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function get($strKey)
	{
		// Init var
		$result = null;
		
		// Try found by key
		try{
			$result = $this->getFromKey($strKey);
		} catch(\Exception $e) {
			// Do nothing
		}
		
		// If key not found: try to get from class path
		if(is_null($result))
		{
			$result = $this->getFromClassPath($strKey);
		}
		
		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     */
    public function getCallable($configFunction, array $tabArg = array())
    {
        // Init var
        $objDepCollection = $this->getObjDependencyCollection();
        $result = $objDepCollection->getCallableFunction($configFunction, $tabArg);

        // Return result
        return $result;
    }



	
	
	// Methods setters
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function setDependencyCollection(DependencyCollectionInterface $objDependencyCollection)
	{
		$this->beanSet(ConstProvider::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION, $objDependencyCollection);
	}





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function call($configFunction, array $tabArg = array())
    {
        // Init var
        $objDepCollection = $this->getObjDependencyCollection();
        $result = $objDepCollection->callFunction($configFunction, $tabArg);

        // Return result
        return $result;
    }



}