<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\library;

use ReflectionFunctionAbstract;
use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\dependency\api\DependencyCollectionInterface;



class ToolBoxDependency
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of arguments,
     * for specified function,
     * from specified dependency collection.
     *
     * Return index array of resolved arguments if able, null else.
     *
     * Setting array of arguments can be sent.
     * Setting argument always set, if argument name valid.
     * Format:
     * Associative array: key = Function parameter name, value = parameter value.
     *
     * Additive array of arguments can be sent.
     * Additive argument used, if set argument, or argument from dependency collection, not found.
     * Each additive argument used only once, in order of setting.
     * Format:
     * Index array of parameter values.
     *
     * @param ReflectionFunctionAbstract $objFunction
     * @param DependencyCollectionInterface $objDependencyCollection
     * @param array $tabSetArg = array()
     * @param array $tabAddArg = array()
     * @param boolean $boolCheckArgValid = true
     * @return null|array
     */
    public static function getTabFunctionArg(
        ReflectionFunctionAbstract $objFunction,
        DependencyCollectionInterface $objDependencyCollection,
        array $tabSetArg = array(),
        array $tabAddArg = array(),
        $boolCheckArgValid = true
    )
    {
        // Init var
        $result = array();
        $boolCheckArgValid = (is_bool($boolCheckArgValid) ? $boolCheckArgValid : true);
        $tabParam = $objFunction->getParameters();
        $tabAddArg = array_values($tabAddArg);
        $boolParamSet = true;

        // Run all function parameters
        for($intCpt = 0; ($intCpt < count($tabParam)) && $boolParamSet; $intCpt++)
        {
            // Get info
            $objParam = $tabParam[$intCpt];
            $strParamNm = $objParam->getName();
            $objParamType = $objParam->getType();
            $strParamType = (
                (!is_null($objParamType)) ?
                    (
                        method_exists($objParamType, 'getName') ?
                            $objParamType->getName() :
                            strval($objParamType)
                    ) :
                    null
            );
            $boolParamSet = false;

            // Get argument: from setting arguments, if found
            if(array_key_exists($strParamNm, $tabSetArg))
            {
                $result[] = $tabSetArg[$strParamNm];
                $boolParamSet = true;
            }

            // Get argument: from dependency collection, if found
            if((!$boolParamSet) && (!is_null($strParamType)))
            {
                $objParamInstance = $objDependencyCollection->getObjInstance($strParamType);

                // Check instance found
                if(!is_null($objParamInstance))
                {
                    $result[] = $objParamInstance;
                    $boolParamSet = true;
                }
            }

            // Get argument: from additive arguments, if found
            if((!$boolParamSet) && (count($tabAddArg) > 0))
            {
                $result[] = array_shift($tabAddArg);
                $boolParamSet = true;
            }

            // Get argument: from default value, if found
            if((!$boolParamSet) && $objParam->isDefaultValueAvailable())
            {
                $result[] = $objParam->getDefaultValue();
                $boolParamSet = true;
            }
        }

        // Check arguments
        if(
            // Check set all arguments
            (!$boolParamSet) ||

            // Check result arguments valid
            (
                $boolCheckArgValid &&
                (!ToolBoxReflection::checkFunctionArgValid($objFunction, $result))
            )
        )
        {
            $result = null;
        }

        // Return result
        return $result;
    }



}