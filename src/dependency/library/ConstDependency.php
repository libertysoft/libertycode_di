<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\library;



class ConstDependency
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Dependency data constants
    const DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION = 'objDependencyCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_OPTION = 'option';
    const TAB_CONFIG_KEY_OPTION_SHARED = 'shared';
    const TAB_CONFIG_KEY_OPTION_CLASS_WIRED = 'class_wired';

    // Collection configuration keys
    const TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE = 'search_cache_only_require';
    const TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH = 'select_class_path';
    const TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE = 'force_access_require';
    const TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE = 'auto_config_require';
    const TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION = 'max_limit_recursion';

    // Collection configuration class path selection
    const CONFIG_SELECT_CLASS_PATH_FIRST = 'first';
    const CONFIG_SELECT_CLASS_PATH_LAST = 'last';



    // Exception message constants
    const EXCEPT_MSG_DEPENDENCY_COLLECTION_INVALID_FORMAT = 'Following dependency collection "%1$s" invalid! It must be a dependency collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default dependency configuration standard.';
	const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a dependency object in collection.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default dependency collection configuration standard.';
    const EXCEPT_MSG_MAX_LIMIT_RECURSION = 'Maximum limit number of recursions "%1$s" over!';
    const EXCEPT_MSG_FUNCTION_ENABLE_CALL = 'Impossible to call following function "%1$s", from following arguments "%2$s"!';



}