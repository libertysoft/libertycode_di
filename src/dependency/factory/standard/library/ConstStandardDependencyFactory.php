<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\factory\standard\library;



class ConstStandardDependencyFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_SERVICE = 'service';
    const CONFIG_TYPE_PREFERENCE = 'preference';
}