<?php
/**
 * Description :
 * This class allows to define standard dependency factory class.
 * Standard dependency factory allows to provide and hydrate dependency instance.
 *
 * Standard dependency factory uses the following specified configuration, to get and hydrate dependency:
 * [
 *     -> Configuration key(optional): "service key"
 *     type(optional): "service",
 *     Service dependency configuration (@see Service )
 *
 *     OR
 *
 *     -> Configuration key(optional): "preference source (if source not found on configuration)"
 *     type(optional: can be automatically detected via set option): "preference",
 *     Preference dependency configuration (@see Preference )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\factory\standard\model;

use liberty_code\di\dependency\factory\model\DefaultDependencyFactory;

use liberty_code\di\dependency\service\library\ConstService;
use liberty_code\di\dependency\service\model\Service;
use liberty_code\di\dependency\preference\library\ConstPreference;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\dependency\factory\library\ConstDependencyFactory;
use liberty_code\di\dependency\factory\standard\library\ConstStandardDependencyFactory;



class StandardDependencyFactory extends DefaultDependencyFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Set preference type, if required
        if(
            (!array_key_exists(ConstDependencyFactory::TAB_CONFIG_KEY_TYPE, $result)) &&
            array_key_exists(ConstPreference::TAB_CONFIG_KEY_SET, $result)
        )
        {
            $result[ConstDependencyFactory::TAB_CONFIG_KEY_TYPE] = ConstStandardDependencyFactory::CONFIG_TYPE_PREFERENCE;
        }

        // Use configuration key, if required
        if(!is_null($strConfigKey))
        {
            // Case preference dependency: configuration key considered as source, if required
            if(
                array_key_exists(ConstDependencyFactory::TAB_CONFIG_KEY_TYPE, $result) &&
                ($result[ConstDependencyFactory::TAB_CONFIG_KEY_TYPE] == ConstStandardDependencyFactory::CONFIG_TYPE_PREFERENCE) &&
                (!array_key_exists(ConstPreference::TAB_CONFIG_KEY_SOURCE, $result))
            )
            {
                $result[ConstPreference::TAB_CONFIG_KEY_SOURCE] = $strConfigKey;
            }
            // Case else: considered as service dependency: configuration key considered as service key, if required
            else if((!array_key_exists(ConstService::TAB_CONFIG_KEY_KEY, $result)))
            {
                $result[ConstService::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrDependencyClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardDependencyFactory::CONFIG_TYPE_SERVICE:
                $result = Service::class;
                break;

            case ConstStandardDependencyFactory::CONFIG_TYPE_PREFERENCE:
                $result = Preference::class;
                break;
        }

        // Return result
        return $result;
    }



}