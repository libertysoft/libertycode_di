<?php
/**
 * Description :
 * This class allows to describe behavior of dependency factory class.
 * Dependency factory allows to provide new or specified dependency instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined dependency types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\factory\api;

use liberty_code\di\dependency\api\DependencyInterface;



interface DependencyFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of dependency,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrDependencyClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance dependency,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param DependencyInterface $objDependency = null
     * @return null|DependencyInterface
     */
    public function getObjDependency(
        array $tabConfig,
        $strConfigKey = null,
        DependencyInterface $objDependency = null
    );
}