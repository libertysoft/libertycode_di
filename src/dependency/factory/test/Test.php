<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/dependency/factory/test/DependencyFactoryTest.php');

// Use
use liberty_code\register\item\instance\model\InstanceCollection;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\service\model\Service;
use liberty_code\di\dependency\preference\model\Preference;



// Init var
$objRegister = new MemoryRegister(new InstanceCollection());
$tabConfig = array(
    'force_access_require' => 1,
    //'auto_config_require' => 1,
    //'max_limit_recursion' => 3
);
$objDependencyCollection = new DefaultDependencyCollection($objRegister, null, $tabConfig);
$objDependencyFactory->setObjDependencyCollection($objDependencyCollection);



// Test new dependency
$tabDependencyData = array(
    [
        [
            'type' => 'service',
            'set' => 'liberty_code\\di\\dependency\\test\\ClassTest6'
        ],
        'liberty_code\\di\\dependency\\test\\ClassTest2'
    ], // Ko: preference config provide for service

    [
        [
            'type' => 'preference',
            'set' => 'liberty_code\\di\\dependency\\test\\ClassTest6'
        ],
        'liberty_code\\di\\dependency\\test\\ClassTest2'
    ], // Ok

    [
        [
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest7',
            'set' =>  ['type' => 'dependency', 'value' => 'svc_4']
        ],
        null,
        new Service()
    ], // Ko: not found: service used for preference config

    [
        [
            'key' => 'pref_2',
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest7',
            'set' =>  ['type' => 'dependency', 'value' => 'svc_4']
        ],
        null,
        new Preference()
    ], // Ok

    [
        [
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
            'argument' => [
                ['type' => 'numeric', 'value' => 1.7],
                ['type' => 'string', 'value' => 'service 1'],
                ['type' => 'boolean', 'value' => 1],
                ['type' => 'array', 'value' => [
                    ['key' => 'key_1', 'type' => 'string', 'value' => 'service 1 array'],
                    ['key' => 'key_2', 'type' => 'numeric', 'value' => 3],
                    ['type' => 'config', 'value' => 'conf_key_3'],
                    ['key' => 'key_4', 'type' => 'array', 'value' => [
                        ['key' => 'key_4_1', 'type' => 'string', 'value' => 'service 1 array array'],
                        ['key' => 'key_4_2', 'type' => 'numeric', 'value' => '7'],
                        ['key' => 'key_4_3', 'type' => 'boolean', 'value' => false]
                    ]
                    ]
                ]
                ],
            ],
            'option' => [
                'shared' => true
            ]
        ],
        'svc_1',
        new Preference()
    ], // Ko: not found: preference used for service config

    [
        [
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
            'argument' => [
                ['type' => 'numeric', 'value' => 1.7],
                ['type' => 'string', 'value' => 'service 1'],
                ['type' => 'boolean', 'value' => 1],
                ['type' => 'array', 'value' => [
                    ['key' => 'key_1', 'type' => 'string', 'value' => 'service 1 array'],
                    ['key' => 'key_2', 'type' => 'numeric', 'value' => 3],
                    ['type' => 'config', 'value' => 'conf_key_3'],
                    ['key' => 'key_4', 'type' => 'array', 'value' => [
                        ['key' => 'key_4_1', 'type' => 'string', 'value' => 'service 1 array array'],
                        ['key' => 'key_4_2', 'type' => 'numeric', 'value' => '7'],
                        ['key' => 'key_4_3', 'type' => 'boolean', 'value' => false]
                    ]
                    ]
                ]
                ],
            ],
            'option' => [
                'shared' => true
            ]
        ],
        'svc_1',
        new Service()
    ], // Ok

    [
        [
            'key' => 'svc_2',
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
            'argument' => [
                ['type' => 'config', 'value' => 'conf_key_2'],
                ['type' => 'string', 'value' => 'service 2'],
                true //,
                //['type' => 'array', 'value' => []] // Test argument with default value
            ]
        ],
        'svc_2_not_care',
        new Service()
    ], // Ok

    [
        [
            'type' => 'service',
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
            'argument' => [
                ['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest5'],
                ['type' => 'callback_return', 'value' => 'new \\DateTime()'],
                ['type' => 'callback_return', 'value' => '9'] //,
                //['type' => 'array', 'value' => []] // Test argument with default value
            ]
        ],
        'svc_3'
    ], // Ok

    [
        [
            'type' => 'preference',
            'key' => 'svc_4',
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
            'argument' => [
                ['type' => 'dependency', 'value' => 'svc_1'],
                ['type' => 'mixed', 'value' => new \DateTime()],
                ['type' => 'boolean', 'value' => '9'] //,
                //['type' => 'array', 'value' => []] // Test argument with default value
            ],
            'option' => [
                'shared' => 1
            ]
        ]
    ], // Ko: service config provide for preference

    [
        [
            'key' => 'svc_4',
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
            'argument' => [
                ['type' => 'dependency', 'value' => 'svc_1'],
                ['type' => 'mixed', 'value' => new \DateTime()],
                ['type' => 'boolean', 'value' => '9'] //,
                //['type' => 'array', 'value' => []] // Test argument with default value
            ],
            'option' => [
                'shared' => 1
            ]
        ]
    ], // Ok

    [
        [
            'source' => 'liberty_code\\di\\dependency\\test\\ClassTest4',
            'argument' => [
                ['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest3']
                //['type' => 'array', 'value' => []] // Test argument with default value
            ],
            'option' => [
                //'shared' => 1
            ]
        ],
        'svc_5'
    ] // Ok
);

foreach($tabDependencyData as $dependencyData)
{
    echo('Test new dependency: <br />');
    echo('<pre>');var_dump($dependencyData);echo('</pre>');

    try{
        $tabConfig = $dependencyData[0];
        $strConfigKey = (isset($dependencyData[1]) ? $dependencyData[1] : null);
        $objDependency = (isset($dependencyData[2]) ? $dependencyData[2] : null);
        $objDependency = $objDependencyFactory->getObjDependency($tabConfig, $strConfigKey, $objDependency);

        echo('Class path: <pre>');var_dump($objDependencyFactory->getStrDependencyClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objDependency))
        {
            echo('Dependency class path: <pre>');var_dump(get_class($objDependency));echo('</pre>');
            echo('Dependency key: <pre>');var_dump($objDependency->getStrKey());echo('</pre>');
            echo('Dependency config: <pre>');var_dump($objDependency->getTabConfig());echo('</pre>');
            echo('Dependency instance call: <pre>');var_dump($objDependency->getStrClassPath());echo('</pre>');
        }
        else
        {
            echo('Dependency not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


