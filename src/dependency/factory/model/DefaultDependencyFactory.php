<?php
/**
 * Description :
 * This class allows to define default dependency factory class.
 * Can be consider is base of all dependency factory type.
 *
 * Default dependency factory uses the following specified configuration, to get and hydrate dependency:
 * [
 *     type(optional): "string constant to determine dependency type",
 *
 *     ... specific dependency configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\factory\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\di\dependency\factory\api\DependencyFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\dependency\api\DependencyInterface;
use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\di\dependency\exception\DependencyCollectionInvalidFormatException;
use liberty_code\di\dependency\factory\library\ConstDependencyFactory;
use liberty_code\di\dependency\factory\exception\FactoryInvalidFormatException;
use liberty_code\di\dependency\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|DependencyCollectionInterface getObjDependencyCollection() Get dependency collection object.
 * @method void setObjDependencyCollection(null|DependencyCollectionInterface $objDependencyCollection) Set dependency collection object.
 * @method null|DependencyFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|DependencyFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultDependencyFactory extends FixBean implements DependencyFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DependencyCollectionInterface $objDependencyCollection = null
     * @param DependencyFactoryInterface $objFactory = null
     */
    public function __construct(
        DependencyCollectionInterface $objDependencyCollection = null,
        DependencyFactoryInterface $objFactory = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init dependency collection
        $this->setObjDependencyCollection($objDependencyCollection);

        // Init dependency factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDependencyFactory::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION))
        {
            $this->__beanTabData[ConstDependencyFactory::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstDependencyFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstDependencyFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDependencyFactory::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION,
            ConstDependencyFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDependencyFactory::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION:
                    DependencyCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstDependencyFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified dependency.
     * Overwrite it to set specific hydration.
     *
     * @param DependencyInterface $objDependency
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateDependency(DependencyInterface $objDependency, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstDependencyFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstDependencyFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate dependency
        $objDependencyCollection = $this->getObjDependencyCollection();
        if(!is_null($objDependencyCollection))
        {
            $objDependency->setDependencyCollection($objDependencyCollection);
        }

        $objDependency->setConfig($tabConfigFormat);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified dependency object.
     *
     * @param DependencyInterface $objDependency
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(DependencyInterface $objDependency, array $tabConfigFormat)
    {
        // Init var
        $strDependencyClassPath = $this->getStrDependencyClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strDependencyClassPath)) &&
            ($strDependencyClassPath == get_class($objDependency))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstDependencyFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstDependencyFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of dependency,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrDependencyClassPathFromType($strConfigType);



    /**
     * Get string class path of dependency engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrDependencyClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrDependencyClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrDependencyClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrDependencyClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrDependencyClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance dependency,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|DependencyInterface
     */
    protected function getObjDependencyNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrDependencyClassPathFromType($strConfigType);
        $result = ToolBoxReflection::getObjInstance($strClassPath);

        // Return result
        return $result;
    }



    /**
     * Get object instance dependency engine.
     *
     * @param array $tabConfigFormat
     * @param DependencyInterface $objDependency = null
     * @return null|DependencyInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjDependencyEngine(
        array $tabConfigFormat,
        DependencyInterface $objDependency = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objDependency = (
            is_null($objDependency) ?
                $this->getObjDependencyNew($strConfigType) :
                $objDependency
        );

        // Get and hydrate dependency, if required
        if(
            (!is_null($objDependency)) &&
            $this->checkConfigIsValid($objDependency, $tabConfigFormat)
        )
        {
            $this->hydrateDependency($objDependency, $tabConfigFormat);
            $result = $objDependency;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjDependency(
        array $tabConfig,
        $strConfigKey = null,
        DependencyInterface $objRoute = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjDependencyEngine($tabConfigFormat, $objRoute);

        // Get dependency from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjDependency($tabConfig, $strConfigKey, $objRoute);
        }

        // Return result
        return $result;
    }



}