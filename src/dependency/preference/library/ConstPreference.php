<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\preference\library;



class ConstPreference
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONF_KEY_PATTERN = 'pref__%1$s';
	const CONF_KEY_REPLACE_CLASS_PASS_SEPARATOR = '_';

    const TAB_CONFIG_KEY_KEY = 'key';
	const TAB_CONFIG_KEY_SOURCE = 'source';
	const TAB_CONFIG_KEY_SET = 'set';
	const TAB_CONFIG_KEY_SET_TYPE = 'type';
	const TAB_CONFIG_KEY_SET_VALUE = 'value';
	
	const TAB_CONFIG_SET_TYPE_CLASS = 'class';
	const TAB_CONFIG_SET_TYPE_DEPENDENCY = 'dependency';
    const TAB_CONFIG_SET_TYPE_INSTANCE = 'instance';
    const TAB_CONFIG_SET_TYPE_CALLBACK_RETURN = 'callback_return';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the preference configuration standard.';
    const EXCEPT_MSG_SET_INVALID_FORMAT = 'Set in config invalid: %1$s! The set must follow the preference configuration standard.';
    const EXCEPT_MSG_INSTANCE_NOT_FOUND = 'Following instance "%1s" not found!';
	


}