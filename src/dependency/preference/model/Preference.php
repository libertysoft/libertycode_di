<?php
/**
 * Description :
 * This class allows to define preference dependency.
 * Preference dependency allows get instance for a specified class.
 *
 * Preference dependency allows to get instance from the following specified configuration:
 * [
 *     Default dependency configuration (@see DefaultDependency ),
 *
 *     key(optional: got @see Preference::getStrKeyDefault() if not found):"service key",
 *
 *     source(required):"class path",
 *
 *     set(required):"class path",
 *
 *     OR
 *
 *     set(required):[
 *         type(optional):"class",
 *         value(required):"string class path"
 *
 *         OR
 *
 *         type(required): "dependency",
 *         value(required): "string dependency key"
 *
 *         OR
 *
 *         type(required): "instance",
 *         value(required): "string serialized object" or object
 *
 *         OR
 *
 *         type(required): "callback_return",
 *         value(required): "string php script (string evaluated as 'return %1$s;')" or callback function
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\preference\model;

use liberty_code\di\dependency\model\DefaultDependency;

use liberty_code\di\dependency\library\ConstDependency;
use liberty_code\di\dependency\preference\library\ConstPreference;
use liberty_code\di\dependency\preference\exception\ConfigInvalidFormatException;
use liberty_code\di\dependency\preference\exception\SetInvalidFormatException;
use liberty_code\di\dependency\preference\exception\InstanceNotFoundException;



class Preference extends DefaultDependency
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
    // ******************************************************************************
    // Methods
    // ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstDependency::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods getters
    // ******************************************************************************

	/**
     * Get string set type.
	 * 
	 * @return string
     */
    protected function getStrSetType()
    {
        // Init var
		$tabConfig = $this->beanGet(ConstDependency::DATA_KEY_DEFAULT_CONFIG);
		
		// Get result
		$result = ConstPreference::TAB_CONFIG_SET_TYPE_CLASS;
		if(isset($tabConfig[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE]))
		{
			$result = $tabConfig[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE];
		}
		
		// Return result
        return $result;
	}
	
	
	
	/**
     * Get set value.
	 * 
	 * @return mixed
     */
    protected function getSetValue()
    {
        // Init var
		$tabConfig = $this->beanGet(ConstDependency::DATA_KEY_DEFAULT_CONFIG);
		
		// Get result
		$result = $tabConfig[ConstPreference::TAB_CONFIG_KEY_SET];
		if(isset($tabConfig[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]))
		{
			$result = $tabConfig[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE];
		}
		
		// Return result
        return $result;
	}



    /**
     * Get string default key.
     *
     * @return string
     */
    protected function getStrKeyDefault()
    {
        // Init var
        $result = sprintf(
            ConstPreference::CONF_KEY_PATTERN,
            str_replace(
                '\\',
                ConstPreference::CONF_KEY_REPLACE_CLASS_PASS_SEPARATOR,
                str_replace(
                    '/',
                    ConstPreference::CONF_KEY_REPLACE_CLASS_PASS_SEPARATOR,
                    $this->getStrClassPathEngine()
                )
            )
        );

        // Return result
        return $result;
    }



	/**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstPreference::TAB_CONFIG_KEY_KEY]) ?
                $tabConfig[ConstPreference::TAB_CONFIG_KEY_KEY] :
                $this->getStrKeyDefault()
        );
		
		// Return result
        return $result;
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getStrClassPathEngine()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstPreference::TAB_CONFIG_KEY_SOURCE];

        // Return result
        return $result;
    }



    /**
     * Get object instance created.
     *
     * @return null|mixed
     * @throws SetInvalidFormatException
     * @throws InstanceNotFoundException
     */
    protected function getObjInstanceNew()
    {
        // Init var
        $result = null;
        $objDependencyCollection = $this->getObjDependencyCollection();
        $strKey = $this->getStrKey();
        $strClassPath = $this->getStrClassPathEngine();
        $strSetType = $this->getStrSetType();
        $setValue = $this->getSetValue();

        // Process by type
        $strErrorMsg = null;
        switch($strSetType)
        {
            // Case get instance by class path
            case ConstPreference::TAB_CONFIG_SET_TYPE_CLASS:
                if($strClassPath != $setValue)
                {
                    $result = $objDependencyCollection->getObjInstance($setValue);
                }
                else
                {
                    $strErrorMsg = 'class invalid';
                }
                break;

            // Case get instance by dependency key
            case ConstPreference::TAB_CONFIG_SET_TYPE_DEPENDENCY:
                $objDependency = $objDependencyCollection->getObjDependency($setValue);
                if((!is_null($objDependency)) && ($strKey != $objDependency->getStrKey()))
                {
                    $result = $objDependency->getObjInstance();
                }
                else
                {
                    $strErrorMsg = 'dependency key invalid';
                }
                break;

            // Case get instance by instance
            case ConstPreference::TAB_CONFIG_SET_TYPE_INSTANCE:

                if(is_object($setValue))
                {
                    $result = $setValue;
                }
                else
                {
                    $obj = @unserialize($setValue);
                    if(is_object($obj))
                    {
                        $result = $obj;
                    }
                    else
                    {
                        $strErrorMsg = 'instance invalid';
                    }
                }
                break;

            // Case get instance by callback
            case ConstPreference::TAB_CONFIG_SET_TYPE_CALLBACK_RETURN:
                try {
                    // Case string: evaluate string code
                    if(is_string($setValue))
                    {
                        $result = eval('return ' . $setValue . ';');
                    }
                    // Case callable: call function
                    else if(is_callable($setValue))
                    {
                        $result = $setValue();
                    }
                } catch (\Exception $e) {
                    $strErrorMsg = 'callback return invalid';
                }
                break;
        }

        // Throw set exception if required
        if(is_string($strErrorMsg) && (trim($strErrorMsg) != ''))
        {
            throw new SetInvalidFormatException($strErrorMsg);
        }
        // Throw exception if getting instance failed
        else if(
            is_null($result) ||
            (!is_object($result)) ||
            (
                (get_class($result) != $strClassPath) &&
                (!is_subclass_of($result, $strClassPath))
            )
        )
        {
            throw new InstanceNotFoundException($setValue);
        }

        // Return result
        return $result;
    }
	
	
	
}