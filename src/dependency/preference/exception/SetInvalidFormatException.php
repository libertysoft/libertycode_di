<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\preference\exception;

use liberty_code\di\dependency\preference\library\ConstPreference;



class SetInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $strMsg
     */
	public function __construct($strMsg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPreference::EXCEPT_MSG_SET_INVALID_FORMAT,
            mb_strimwidth(strval($strMsg), 0, 50, "...")
        );
	}
}