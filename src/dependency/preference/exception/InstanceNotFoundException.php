<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\preference\exception;

use liberty_code\di\dependency\preference\library\ConstPreference;



class InstanceNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $strClassSelector
     */
	public function __construct($strClassSelector)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPreference::EXCEPT_MSG_INSTANCE_NOT_FOUND,
            mb_strimwidth(strval($strClassSelector), 0, 50, "...")
        );
	}
}