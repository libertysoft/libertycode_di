<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\preference\exception;

use liberty_code\di\dependency\preference\library\ConstPreference;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPreference::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
			(is_array($config) && (count($config) > 0)) &&

            // Check valid string key
            (
                (!isset($config[ConstPreference::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstPreference::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstPreference::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid source (class or interface)
			isset($config[ConstPreference::TAB_CONFIG_KEY_SOURCE]) &&
            is_string($config[ConstPreference::TAB_CONFIG_KEY_SOURCE]) &&
            (trim($config[ConstPreference::TAB_CONFIG_KEY_SOURCE]) != '') &&
            (
                class_exists($config[ConstPreference::TAB_CONFIG_KEY_SOURCE]) ||
                interface_exists($config[ConstPreference::TAB_CONFIG_KEY_SOURCE])
            ) &&

            // Check valid set
            isset($config[ConstPreference::TAB_CONFIG_KEY_SET]) &&
			(
                // Case: valid string
                (
                    is_string($config[ConstPreference::TAB_CONFIG_KEY_SET]) &&
                    (trim($config[ConstPreference::TAB_CONFIG_KEY_SET]) != '')
                ) ||

				// Case: valid array
				(
                    isset($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]) &&

                    (
                        // Check default type class
                        (
                            (!isset($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE])) &&
                            is_string($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]) &&
                            (trim($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]) != '')
                        ) ||
                        (
                            isset($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE]) &&
                            (
                                // Check valid type and value, for class and dependency
                                (
                                    (
                                        ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                            ConstPreference::TAB_CONFIG_SET_TYPE_CLASS) ||
                                        ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                            ConstPreference::TAB_CONFIG_SET_TYPE_DEPENDENCY) ||
                                        ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                            ConstPreference::TAB_CONFIG_SET_TYPE_INSTANCE) ||
                                        ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                            ConstPreference::TAB_CONFIG_SET_TYPE_CALLBACK_RETURN)
                                    )
                                     &&
                                    is_string($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]) &&
                                    (trim($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE]) != '')
                                ) ||

                                // Check valid type and value, for instance
                                (
                                    ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                        ConstPreference::TAB_CONFIG_SET_TYPE_INSTANCE) &&
                                    is_object($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE])
                                ) ||

                                // Check valid type and value, for callback
                                (
                                    ($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_TYPE] ==
                                        ConstPreference::TAB_CONFIG_SET_TYPE_CALLBACK_RETURN) &&
                                    is_callable($config[ConstPreference::TAB_CONFIG_KEY_SET][ConstPreference::TAB_CONFIG_KEY_SET_VALUE])
                                )
                            )
                        )

                    )
				)
			);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}