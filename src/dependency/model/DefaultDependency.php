<?php
/**
 * Description :
 * This class allows to define default dependency class.
 * Can be consider is base of all dependency type.
 *
 * Default dependency uses the following specified configuration:
 * [
 *     option(optional):[
 *         shared(optional: got false if not found): true / false,
 *         class_wired(optional: got true if not found): true / false
 *     ]
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\di\dependency\api\DependencyInterface;

use liberty_code\di\config\model\DefaultConfig;
use liberty_code\di\dependency\library\ConstDependency;
use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\di\dependency\exception\ConfigInvalidFormatException;
use liberty_code\di\dependency\exception\DependencyCollectionInvalidFormatException;



abstract class DefaultDependency extends FixBean implements DependencyInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param DependencyCollectionInterface $objDependencyCollection = null
     */
    public function __construct(array $tabConfig = null, DependencyCollectionInterface $objDependencyCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init dependency collection if required
        if(!is_null($objDependencyCollection))
        {
            $this->setDependencyCollection($objDependencyCollection);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION))
        {
            $this->__beanTabData[ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstDependency::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstDependency::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION,
            ConstDependency::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION:
                    DependencyCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstDependency::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if dependency is shared.
     *
     * @return boolean
     */
    public function checkIsShared()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = false;

        if(
            array_key_exists(ConstDependency::TAB_CONFIG_KEY_OPTION, $tabConfig) &&
            array_key_exists(ConstDependency::TAB_CONFIG_KEY_OPTION_SHARED, $tabConfig[ConstDependency::TAB_CONFIG_KEY_OPTION])
        )
        {
            $result = (intval($tabConfig[ConstDependency::TAB_CONFIG_KEY_OPTION]
                [ConstDependency::TAB_CONFIG_KEY_OPTION_SHARED]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if dependency is class wired (detectable by class).
     *
     * @return boolean
     */
    public function checkIsClassWired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = true;

        if(
            array_key_exists(ConstDependency::TAB_CONFIG_KEY_OPTION, $tabConfig) &&
            array_key_exists(ConstDependency::TAB_CONFIG_KEY_OPTION_CLASS_WIRED, $tabConfig[ConstDependency::TAB_CONFIG_KEY_OPTION])
        )
        {
            $result = (intval($tabConfig[ConstDependency::TAB_CONFIG_KEY_OPTION]
                [ConstDependency::TAB_CONFIG_KEY_OPTION_CLASS_WIRED]) != 0);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get dependency configuration object.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
     * @inheritdoc
     */
    public function getObjDependencyCollection()
    {
        // Return result
        return $this->beanGet(ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstDependency::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get string class path of instance.
     *
     * @return string
     */
    abstract protected function getStrClassPathEngine();



    /**
     * @inheritdoc
     */
    public function getStrClassPath()
    {
        // Init var
        $result = null;

        // Get class from source if required
        if($this->checkIsClassWired())
        {
            $result = $this->getStrClassPathEngine();
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance created.
     *
     * @return null|mixed
     */
    abstract protected function getObjInstanceNew();



    /**
     * @inheritdoc
     */
    public function getObjInstance()
    {
        // Init var
        $result = null;
        $objDependencyCollection = $this->getObjDependencyCollection();
        $objRegister = $objDependencyCollection->getObjRegister();
        $boolShared = (
            (!is_null($objRegister)) &&
            $this->checkIsShared()
        );

        // Get instance from register, if possible
        if($boolShared)
        {
            $result = $objRegister->getItem($this->getStrKey());
        }

        // Get new instance, if required (not found in register)
        if(is_null($result))
        {
            $result = $this->getObjInstanceNew();
        }

        // Register instance, if required
        if(
            $boolShared &&
            (!is_null($result))
        )
        {
            $objRegister->putItem($this->getStrKey(), $result);
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************
	
	/**
     * @inheritdoc
	 */
	public function setDependencyCollection(DependencyCollectionInterface $objDependencyCollection = null)
	{
		$this->beanSet(ConstDependency::DATA_KEY_DEFAULT_DEPENDENCY_COLLECTION, $objDependencyCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstDependency::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}