<?php
/**
 * Description :
 * This class allows to define default dependency collection class.
 * key: dependency key => dependency object.
 *
 * Default dependency collection uses the following specified configuration:
 * [
 *     search_cache_only_require(optional: got false if not found): true / false,
 *
 *     select_class_path(optional: got first if not found): 'first' / 'last',
 *
 *     force_access_require(optional: got false if not found): true / false,
 *
 *     auto_config_require(optional: got default configuration value if not found): true / false,
 *
 *     max_limit_recursion(optional: got default configuration value if not found):
 *         positive integer (0 means no limit)
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\di\dependency\api\DependencyCollectionInterface;

use ReflectionClass;
use liberty_code\library\bean\library\ConstBean;
use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\di\config\model\DefaultConfig;
use liberty_code\di\dependency\library\ConstDependency;
use liberty_code\di\dependency\library\ToolBoxDependency;
use liberty_code\di\dependency\api\DependencyInterface;
use liberty_code\di\dependency\exception\CollectionConfigInvalidFormatException;
use liberty_code\di\dependency\exception\CollectionKeyInvalidFormatException;
use liberty_code\di\dependency\exception\CollectionValueInvalidFormatException;
use liberty_code\di\dependency\exception\MaxLimitRecursionException;
use liberty_code\di\dependency\exception\FunctionEnableCallException;



class DefaultDependencyCollection extends DefaultBean implements DependencyCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	/** @var RegisterInterface */
	protected $objRegister;



    /** @var ConfigInterface */
    protected $objConfigExt;



    /** @var array */
    protected $tabConfig;

	
	
	/** 
	 * Associative array (key: dependency instance class path => value: dependency key).
	 * Allows to search faster.
	 * @var array 
	 */
	protected $tabClassPath;
	
	
	
	/** 
	 * Count of recursion during dependency search.
	 * @var null|int
	 */
	protected $intCountRecursion;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
	 * @param RegisterInterface $objRegister = null
     * @param ConfigInterface $objConfigExt = null
     * @param array $tabConfig = null
     * @param array $tabData = array()
     */
	public function __construct(
	    RegisterInterface $objRegister = null,
        ConfigInterface $objConfigExt = null,
        array $tabConfig = null,
        array $tabData = array()
    )
	{
		// Init var
        $this->objRegister = null;
        $this->objConfigExt = null;
        $this->tabConfig = array();
		$this->tabClassPath = array();
		$this->intCountRecursion = null;
		
		// Call parent constructor
		parent::__construct($tabData);
		
		// Init register, if required
		if(!is_null($objRegister))
		{
			$this->setRegister($objRegister);
		}

        // Init external configuration, if required
        if(!is_null($objConfigExt))
        {
            $this->setConfigExt($objConfigExt);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			// Check value argument
			CollectionValueInvalidFormatException::setCheck($value);

			// Check key argument
			/** @var DependencyInterface $value */
			if(
				(!is_string($key)) ||
				($key != $value->getStrKey())
			)
			{
				throw new CollectionKeyInvalidFormatException($key);
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}




	
	// Methods check
	// ******************************************************************************

    /**
     * Check search cache only only required.
     *
     * @return boolean
     */
    protected function checkSearchCacheOnlyRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE]) &&
            (intval($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check select class path first option required,
     * for dependency search.
     *
     * @return boolean
     */
    public function checkSelectClassPathFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH])) ||
            ($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH] ==
                ConstDependency::CONFIG_SELECT_CLASS_PATH_FIRST)
        );

        // Return result
        return $result;
    }



    /**
     * Check select class path last option required,
     * for dependency search.
     *
     * @return boolean
     */
    public function checkSelectClassPathLastRequired()
    {
        // Return result
        return (!$this->checkSelectClassPathFirstRequired());
    }



    /**
     * @inheritdoc
     */
    public function checkForceAccessRequired()
    {
        // Init var
        $result = false;
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if auto-configuration option required.
     * This option allows to create a dependency instance automatically without dependency configuration.
     *
     * @return boolean
     */
    public function checkAutoConfigRequired()
    {
        // Init var
        $objConfig = $this->getObjConfig();
        $result = $objConfig->checkIsAutoConfig();
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



	/** 
	 * @inheritdoc
	 */
	public function checkDependencyExists($strKey)
	{
		// Return result
		return $this->beanDataExists($strKey);
	}



	/**
	 * @inheritdoc
	 */
	public function checkDependencyExistsFromClassPath($strClassPath)
	{
		// Return result
		return (!is_null($this->getObjDependencyFromClassPath($strClassPath)));
	}
	
	
	
	/**
	 * @inheritdoc
	 */
	public function checkInstanceExists($strClassPath)
	{
		// Return result
		return (!is_null($this->getObjInstance($strClassPath)));
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get dependency configuration object.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



	/**
	 * @inheritdoc
	 */
	public function getObjRegister()
	{
		// Return result
		return $this->objRegister;
	}



    /**
     * @inheritdoc
     */
    public function getObjConfigExt()
    {
        // Return result
        return $this->objConfigExt;
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * Get maximum limit of recursions for dependency search.
     *
     * @return integer
     */
    public function getIntMaxLimitRecursion()
    {
        // Init var
        $objConfig = $this->getObjConfig();
        $result = $objConfig->getIntMaxLimitRecursion();
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION, $tabConfig))
        {
            $result = $tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION];
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getTabKey()
	{
		// Return result
		return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
	}



	/**
	 * @inheritdoc
	 */
	public function getObjDependency($strKey)
	{
		// Init var
		$result = null;
		
		// Get dependency if found
		if($this->checkDependencyExists($strKey))
		{
			$result = $this->beanGetData($strKey);
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
     * Use select_class_path configuration option to select item,
     * in case of multi-matches.
     * Order of dependencies is important.
     *
	 * @inheritdoc
	 */
	public function getObjDependencyFromClassPath($strClassPath)
	{
		// Init var
		$result = null;
		
		// Try to get dependency from class path in cache
		if(array_key_exists($strClassPath, $this->tabClassPath))
		{
			// Get info
			$strKey = $this->tabClassPath[$strClassPath];
			$objDependency = $this->getObjDependency($strKey);

            // Get dependency, if required (dependency class valid)
			if(!is_null($objDependency))
            {
				$strDepClassPath = $objDependency->getStrClassPath();
                $result = (
                    (
                        (!is_null($strDepClassPath)) &&
                        ($strClassPath === $strDepClassPath)
                    ) ?
                        $objDependency :
                        $result
                );
            }
			
			// If dependency not found, remove class path from cache
			if(is_null($result))
			{
				unset($this->tabClassPath[$strClassPath]);
			}
		}
		
		// If not found in cache, try to find dependency from collection, if required
		if((!$this->checkSearchCacheOnlyRequired()) && is_null($result))
		{
            // Run all dependencies
            $boolSelectClassPathFirst = $this->checkSelectClassPathFirstRequired();
            /** @var DependencyInterface[] $tabDependency */
            $tabDependency = $this->__beanTabData; // Perf requirement: Get directly array of dependencies
            foreach($tabDependency as $strKey => $objDependency)
            {
                // Get info
                $strDependencyClassPath = $objDependency->getStrClassPath();

                // Check class path found
                if(
                    (!is_null($strDependencyClassPath)) &&
                    ($strClassPath === $strDependencyClassPath)
                )
                {
                    // Set result and register in cache
                    $result = $objDependency;
                    $this->tabClassPath[$strClassPath] = $strKey;

                    // Break, if required
                    if($boolSelectClassPathFirst) break;
                }
            }
		}
		
		// Return result
		return $result;
	}
	
	
	
	/**
	 * @inheritdoc
     * @throws MaxLimitRecursionException
	 */
	public function getObjInstance($strClassPath)
	{
        // Init var
        $result = null;
        $intMaxLimitRecursion = $this->getIntMaxLimitRecursion();

        // Start count of recursions, if required
        $boolStartCountRecursion = (!is_int($this->intCountRecursion));
        if($boolStartCountRecursion)
        {
            $this->intCountRecursion = 0;
        }

        // Increment number of recursions
        $this->intCountRecursion++;

        // Check number of recursions
        $boolCountRecursion = (
            ($intMaxLimitRecursion == 0) ||
            ($this->intCountRecursion <= $intMaxLimitRecursion)
        );
        if($boolCountRecursion)
        {
            // Get info
            $objDependency = $this->getObjDependencyFromClassPath($strClassPath);

            // Check dependency found
            if(!is_null($objDependency))
            {
                $result = $objDependency->getObjInstance();
            }
            // Else, if class path found: try to instantiate constructor arguments
            else if($this->checkAutoConfigRequired() && class_exists($strClassPath))
            {
                // Check class instantiable
                $objClass = new ReflectionClass($strClassPath);
                if(
                    (!$objClass->isAbstract()) &&
                    (!$objClass->isInterface())
                )
                {
                    // Get info
                    $objConstruct = $objClass->getConstructor();

                    // Get arguments, if constructor found
                    $tabArg = array();
                    if(!is_null($objConstruct))
                    {
                        $tabArg = ToolBoxDependency::getTabFunctionArg(
                            $objConstruct,
                            $this,
                            array(),
                            array(),
                            false
                        );
                        $tabArg = (is_array($tabArg) ? $tabArg : array());
                    }

                    // Create instance
                    $result = ToolBoxReflection::getObjInstance($strClassPath, $tabArg, $this->checkForceAccessRequired());
                }
            }
        }

        // Close count of recursions, if required
        if($boolStartCountRecursion)
        {
            $this->intCountRecursion = null;
        }

        // Throw exception if number of recursions over
        if(!$boolCountRecursion)
        {
            throw new MaxLimitRecursionException($intMaxLimitRecursion);
        }

        // Return result
        return $result;
	}



    /**
     * @inheritdoc
     * Try to auto-determine arguments from dependency collection.
     *
     * Optional array of arguments considered as additional.
     * Array split in 2 following arrays, depending of format.
     * Format:
     * - Setting array of arguments format:
     *   @see ToolBoxDependency::getTabFunctionArg() , setting array of arguments section.
     * - Additive array of arguments format:
     *   @see ToolBoxDependency::getTabFunctionArg() , additive array of arguments section.
     */
    public function getCallableFunction($configFunction, array $tabArg = array())
    {
        // Init var
        $result = null;
        $objFunction = ToolBoxReflection::getObjFunction($configFunction);
        $tabSetArg = array();
        $tabAddArg = array();

        // Check reflection function object found
        if(!is_null($objFunction))
        {
            // Get setting and additive arguments
            foreach($tabArg as $key => $value)
            {
                if(is_string($key))
                {
                    $tabSetArg[$key] = $value;
                }
                else if(is_integer($key))
                {
                    $tabAddArg[] = $value;
                }
            }

            // Get function arguments
            $tabFunctionArg = ToolBoxDependency::getTabFunctionArg(
                $objFunction,
                $this,
                $tabSetArg,
                $tabAddArg
            );

            // Check function arguments found
            if(!is_null($tabFunctionArg))
            {
                // Get callable
                $result = ToolBoxReflection::getCallFunction(
                    $configFunction,
                    $tabFunctionArg,
                    $this->checkForceAccessRequired()
                );
            }
        }

        // Return result
        return $result;
    }
	



	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function setRegister(RegisterInterface $objRegister)
	{
		$this->objRegister = $objRegister;
	}



    /**
     * @inheritdoc
     */
    public function setConfigExt(ConfigInterface $objConfigExt)
    {
        $this->objConfigExt = $objConfigExt;
    }



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



    /**
     * @inheritdoc
     * @throws CollectionConfigInvalidFormatException
     */
    public function setForceAccessRequired($boolForceAccessRequire)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set force access option
        $tabConfig[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE] = $boolForceAccessRequire;

        // Set configuration
        $this->setConfig($tabConfig);
    }


	
	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setDependency(DependencyInterface $objDependency)
	{
		// Init var
		$strKey = $objDependency->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objDependency);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabDependency($tabDependency)
    {
        // Init var
        $result = array();

        // Case index array of dependencies
        if(is_array($tabDependency))
        {
            // Run all dependencies and for each, try to set
            foreach($tabDependency as $dependency)
            {
                $strKey = $this->setDependency($dependency);
                $result[] = $strKey;
            }
        }
        // Case collection of dependencies
        else if($tabDependency instanceof DependencyCollectionInterface)
        {
            // Run all dependencies and for each, try to set
            foreach($tabDependency->getTabKey() as $strKey)
            {
                $objDependency = $tabDependency->getObjDependency($strKey);
                $strKey = $this->setDependency($objDependency);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeDependency($strKey)
    {
        // Init var
        $result = $this->getObjDependency($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeDependencyAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeDependency($strKey);
        }
    }



	/**
	 * Remove a specified dependency from register.
	 * If no dependency specified, remove all dependencies from register.
	 *
	 * @param string $strKey = null
	 * @return boolean
	 */
	protected function removeRegisterDependency($strKey = null)
	{
		// Init var
		$result = false;
		$objRegister = $this->getObjRegister();

		// Check register found
		if(!is_null($objRegister))
		{
			// Remove a specified item from register
			if(!is_null($strKey))
			{
				if($objRegister->checkItemExists($strKey))
				{
					$objRegister->removeItem($strKey);
					$result = true;
				}
			}
			// Else: remove all item from register
			else
			{
				$objRegister->removeItemAll();
				$result = true;
			}
		}

		// Return result
		return $result;
	}



	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register dependency collection on dependency object
		/** @var DependencyInterface $val */
		$val->setDependencyCollection($this);

		// Remove from register, if required
		$this->removeRegisterDependency($key);

        // Register class path in cache
        $strClassPath = $val->getStrClassPath();
        if(
            $this->checkSelectClassPathLastRequired() ||
            (!array_key_exists($strClassPath, $this->tabClassPath))
        )
        {
            $this->tabClassPath[$strClassPath] = $key;
        }
    }



	/**
	 * @inheritdoc
	 */
	protected function beanRemove($key)
	{
        // Remove class path from cache, if required
        $this->tabClassPath = array_diff($this->tabClassPath, array($key));

		// Remove from register, if required
		$this->removeRegisterDependency($key);

		// Call parent method
		parent::beanRemove($key);
	}





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * Try to auto-determine arguments from dependency collection.
     *
     * Optional array of arguments format:
     * @see DefaultDependencyCollection::getCallableFunction() .
     *
     * @throws FunctionEnableCallException
     */
    public function callFunction($configFunction, array $tabArg = array())
    {
        // Get callable
        $callable = $this->getCallableFunction($configFunction, $tabArg);

        // Check callable found
        if(is_null($callable))
        {
            throw new FunctionEnableCallException($configFunction, $tabArg);
        }

        // Return result
        return $callable();
    }



}