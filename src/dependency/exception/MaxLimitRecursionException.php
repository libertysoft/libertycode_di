<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\exception;

use liberty_code\di\dependency\library\ConstDependency;



class MaxLimitRecursionException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param int $intLimitRecursion
     */
	public function __construct($intLimitRecursion)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstDependency::EXCEPT_MSG_MAX_LIMIT_RECURSION, strval($intLimitRecursion));
	}
	
	
	
}