<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\exception;

use liberty_code\di\dependency\library\ConstDependency;



class FunctionEnableCallException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $configFunction
     * @param array $tabArg
     */
	public function __construct($configFunction, array $tabArg)
	{
		// Call parent constructor
		parent::__construct();

		// Init var
        $strConfigFunction = strval(is_array($configFunction) ? serialize($configFunction) : $configFunction);
        $strTabArg = serialize($tabArg);
		$this->message = sprintf
        (
            ConstDependency::EXCEPT_MSG_FUNCTION_ENABLE_CALL,
            mb_strimwidth($strConfigFunction, 0, 50, "..."),
            mb_strimwidth($strTabArg, 0, 50, "...")
        );
	}
}