<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\exception;

use liberty_code\di\dependency\library\ConstDependency;



class CollectionConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDependency::EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid search cache only required option
            (
                (!isset($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE]) ||
                    is_int($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE]) ||
                    (
                        is_string($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE]) &&
                        ctype_digit($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SEARCH_CACHE_ONLY_REQUIRE])
                    )
                )
            ) &&

            // Check valid select class path option
            (
                (!isset($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH])) ||
                (
                    // Check is valid option
                    is_string($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH]) &&
                    (
                        ($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH] ==
                            ConstDependency::CONFIG_SELECT_CLASS_PATH_FIRST) ||
                        ($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_SELECT_CLASS_PATH] ==
                            ConstDependency::CONFIG_SELECT_CLASS_PATH_LAST)
                    )
                )
            ) &&

            // Check valid force access required option
            (
                (!isset($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE]) ||
                    is_int($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE]) ||
                    (
                        is_string($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE]) &&
                        ctype_digit($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_FORCE_ACCESS_REQUIRE])
                    )
                )
            ) &&

            // Check valid auto-configuration required option
            (
                (!isset($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE]) ||
                    is_int($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE]) ||
                    (
                        is_string($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE]) &&
                        ctype_digit($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_AUTO_CONFIG_REQUIRE])
                    )
                )
            ) &&

            // Check valid maximum limit number of recursions
            (
                (!isset($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION])) ||
                (
                    is_int($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION]) &&
                    ($config[ConstDependency::TAB_COLLECTION_CONFIG_KEY_MAX_LIMIT_RECURSION] >= 0)
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}