<?php
/**
 * Description :
 * This class allows to describe behavior of dependency collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\api;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\api\ConfigInterface;
use liberty_code\di\dependency\api\DependencyInterface;



interface DependencyCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

    /**
     * Check if force access option required.
     * This option allows to force access during instantiation,
     * in case of private or protected constructor/method.
     *
     * @return boolean
     */
    public function checkForceAccessRequired();



	/**
	 * Check if dependency exists from specified key.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkDependencyExists($strKey);
	
	
	
	/**
	 * Check if dependency exists from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return boolean
	 */
	public function checkDependencyExistsFromClassPath($strClassPath);
	
	
	
	/**
	 * Check if instance exists from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return boolean
	 */
	public function checkInstanceExists($strClassPath);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get register object.
	 * 
	 * @return null|RegisterInterface
	 */
	public function getObjRegister();



    /**
     * Get external configuration object.
     *
     * @return null|ConfigInterface
     */
    public function getObjConfigExt();



	/**
	 * Get index array of keys.
	 *
	 * @return array
	 */
	public function getTabKey();



	/**
	 * Get dependency from specified key.
	 * 
	 * @param string $strKey
	 * @return null|DependencyInterface
	 */
	public function getObjDependency($strKey);
	
	
	
	/**
	 * Get dependency from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return null|DependencyInterface
	 */
	public function getObjDependencyFromClassPath($strClassPath);
	
	
	
	/**
	 * Get instance object from specified class path.
	 * 
	 * @param string $strClassPath
	 * @return null|mixed
	 */
	public function getObjInstance($strClassPath);



    /**
     * Get callback function,
     * to execute specified function configuration, and return result, if required.
     * It can use specified optional array of arguments, if provided.
     *
     * Function configuration format:
     * @see ToolBoxReflection::getCallFunction() .
     *
     * @param mixed $configFunction
     * @param array $tabArg = array()
     * @return null|callable
     */
    public function getCallableFunction($configFunction, array $tabArg = array());
	




	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set register object.
	 * 
	 * @param RegisterInterface $objRegister
	 */
	public function setRegister(RegisterInterface $objRegister);



    /**
     * Set external configuration object.
     *
     * @param ConfigInterface $objConfigExt
     */
    public function setConfigExt(ConfigInterface $objConfigExt);



    /**
     * Set force access required option.
     * This option allows to force access during instantiation,
     * in case of private or protected constructor/method.
     *
     * @param boolean $boolForceAccessRequire
     */
    public function setForceAccessRequired($boolForceAccessRequire);


	
	/**
	 * Set dependency and return its key.
	 * 
	 * @param DependencyInterface $objDependency
	 * @return string
     */
	public function setDependency(DependencyInterface $objDependency);



    /**
     * Set list of dependencies (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|DependencyCollectionInterface $tabDependency
     * @return array
     */
    public function setTabDependency($tabDependency);



    /**
     * Remove dependency and return its instance.
     *
     * @param string $strKey
     * @return DependencyInterface
     */
    public function removeDependency($strKey);



    /**
     * Remove all dependencies.
     */
    public function removeDependencyAll();





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified function configuration,
     * and return result, if required.
     * It can use specified optional array of arguments, if provided.
     *
     * Function configuration format:
     * @see DependencyCollectionInterface::getCallableFunction() .
     *
     * @param mixed $configFunction
     * @param array $tabArg = array()
     * @return mixed
     */
    public function callFunction($configFunction, array $tabArg = array());
}