<?php
/**
 * Description :
 * This class allows to describe behavior of dependency class.
 * Dependency is item, containing all information to instantiate a specified class and get object.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\api;

use liberty_code\di\dependency\api\DependencyCollectionInterface;



interface DependencyInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get dependency collection object.
	 *
	 * @return null|DependencyCollectionInterface
	 */
	public function getObjDependencyCollection();
	
	
	
	/**
	 * Get string key (considered as dependency id).
	 *
	 * @return string
	 */
	public function getStrKey();

	
	
	/**
	 * Get string class path of instance.
	 *
	 * @return null|string
	 */
	public function getStrClassPath();


	
    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get instance object.
     *
     * @return mixed
     */
    public function getObjInstance();
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set dependency collection object.
	 * 
	 * @param DependencyCollectionInterface $objDependencyCollection = null
	 */
	public function setDependencyCollection(DependencyCollectionInterface $objDependencyCollection = null);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}