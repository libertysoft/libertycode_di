<?php
/**
 * Description :
 * This class allows to define service dependency.
 * Service dependency allows create instance for a specified class.
 *
 * Service dependency allows to get instance from the following specified configuration:
 * [
 *     Default dependency configuration (@see DefaultDependency ),
 *
 *     key(required):"service key",
 *
 *     source(required):"class path",
 *
 *     argument(optional):[
 *         [
 *             mixed value
 *
 *             OR
 *
 *             type: "mixed",
 *             value: mixed value
 *
 *             OR
 *
 *             type: "string",
 *             value: "string value"
 *
 *             OR
 *
 *             type: "numeric",
 *             value: numeric value or "string numeric value"
 *
 *             OR
 *
 *             type: "boolean",
 *             value: true / false or 0 / 1
 *
 *             OR
 *
 *             type: "array",
 *             value: [[key:"key 1", type:"type 1", value:"value 1"], ...]
 *
 *             OR
 *
 *             type: "config",
 *             value: "string config key"
 *
 *             OR
 *
 *             type: "class",
 *             value: "string class path"
 *
 *             OR
 *
 *             type: "dependency",
 *             value: "string dependency key"
 *
 *             OR
 *
 *             type: "callback_return",
 *             value: "string php script (string evaluated as 'return %1$s;')" or callback function
 *         ],
 *         ...
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\service\model;

use liberty_code\di\dependency\model\DefaultDependency;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\dependency\library\ConstDependency;
use liberty_code\di\dependency\service\library\ConstService;
use liberty_code\di\dependency\service\exception\ConfigInvalidFormatException;
use liberty_code\di\dependency\service\exception\ArgumentInvalidFormatException;
use liberty_code\di\dependency\service\exception\InstanceEnableCreateException;



class Service extends DefaultDependency
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstDependency::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
					break;

				default:
					$result = parent::beanCheckValidValue($key, $value, $error);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
     * Get index array of configured arguments.
	 *
	 * @param array $tabConfigArg
	 * @return array
     * @throws ArgumentInvalidFormatException
     */
    protected function getTabArg(array $tabConfigArg)
	{
		// Init var
		$result = array();
		$objDependencyCollection = $this->getObjDependencyCollection();
        $strKey = $this->getStrKey();
		$strClassPath = $this->getStrClassPathEngine();
		
		// Run all arguments
		foreach($tabConfigArg as $tabArgInfo)
		{
			// Get argument type
			$strType = ConstService::TAB_CONFIG_ARGUMENT_TYPE_MIXED;
			if(isset($tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_TYPE]))
			{
				$strType = $tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_TYPE];
			}
			
			// Get argument value
			$value = $tabArgInfo;
            if(isset($tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_TYPE]))
			{
                if(array_key_exists(ConstService::TAB_CONFIG_KEY_ARGUMENT_VALUE, $tabArgInfo))
				{
					$value = $tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_VALUE];
				}
				else
				{
					throw new ArgumentInvalidFormatException('value unfound for ' . strval($strType));
				}
			}
			
			// Process by argument type
			$processValue = null;
			$strErrorMsg = null;
			switch($strType)
			{
                case ConstService::TAB_CONFIG_ARGUMENT_TYPE_MIXED:
                    $processValue = $value;
                    break;

				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_STRING:
					if(is_string($value))
					{
						$processValue = $value;
					}
					else
					{
						$strErrorMsg = 'string invalid';
					}
					break;
				
				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_NUM:
					if(is_int($value) || is_double($value) || is_float($value))
					{
						$processValue = $value;
					}
					else if(ctype_digit($value))
					{
						$processValue = intval($value);
					}
					else if(is_numeric($value))
					{
						$processValue = floatval($value);
					}
					else
					{
						$strErrorMsg = 'number invalid';
					}
					break;
					
				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_BOOL:
					if(is_bool($value))
					{
						$processValue = $value;
					}
					else if(is_int($value) || (is_string($value) && ctype_digit($value)))
					{
						$processValue = (intval($value) != 0);
					}
					else
					{
						$strErrorMsg = 'boolean invalid';
					}
					break;
					
				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_ARRAY:
					if(is_array($value))
					{
						// Run all items
						$processValue = $this->getTabArg($value);
					}
					else
					{
						$strErrorMsg = 'array invalid';
					}
					break;

                case ConstService::TAB_CONFIG_ARGUMENT_TYPE_CONFIG:
                    $objConfigExt = $objDependencyCollection->getObjConfigExt();
                    if(
                        (!is_null($objConfigExt)) &&
                        $objConfigExt->checkValueExists($value)
                    )
                    {
                        $processValue = $objConfigExt->getValue($value);
                    }
                    else
                    {
                        $strErrorMsg = 'configuration key invalid';
                    }
                    break;

				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_CLASS:
					if(is_string($value) && ($value != $strClassPath))
					{
						$processValue = $objDependencyCollection->getObjInstance($value);
					}
					
					if(is_null($processValue))
					{
						$strErrorMsg = 'class invalid';
					}
					break;
					
				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_DEPENDENCY:
					$objDependency = null;
					if(is_string($value))
					{
						$objDependency = $objDependencyCollection->getObjDependency($value);
					}
					
					if((!is_null($objDependency)) && ($strKey != $objDependency->getStrKey()))
					{
						$processValue = $objDependency->getObjInstance();
					}
					else
					{
						$strErrorMsg = 'dependency key invalid';
					}
					break;
					
				case ConstService::TAB_CONFIG_ARGUMENT_TYPE_CALLBACK_RETURN:
					try {
					    // Case string: evaluate string code
					    if(is_string($value))
                        {
                            $processValue = eval('return ' . $value . ';');
                        }
                        // Case else: considered as callback function
					    else
                        {
                            $processValue = $value();
                        }
					} catch (\Exception $e) {
						$strErrorMsg = 'callback return invalid';
					}
					break;
					
				default:
                    throw new ArgumentInvalidFormatException('type "' . strval($strType) . '"invalid format');
					break;
			}
			
			// Throw value exception if required
			if(is_string($strErrorMsg) && (trim($strErrorMsg) != ''))
			{
                throw new ArgumentInvalidFormatException('value invalid format for type "' . strval($strType) . '": '.$strErrorMsg);
			}
			
			// Register value: case key unfound: insert as associative array
			if(
                isset($tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_KEY]) &&
                is_string($tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_KEY]) &&
                (trim($tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_KEY]) != '')
            )
			{
				$result[$tabArgInfo[ConstService::TAB_CONFIG_KEY_ARGUMENT_KEY]] = $processValue;
			}
			// Register value: case else key unfound: insert as index array
			else
			{
				$result[] = $processValue;
			}
		}
		
		// Return result
        return $result;
	}
	
	
	
	/**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
		$tabConfig = $this->getTabConfig();
		$result = $tabConfig[ConstService::TAB_CONFIG_KEY_KEY];
		
		// Return result
        return $result;
    }
	
	
	
	/**
	 * @inheritdoc
	 */
    protected function getStrClassPathEngine()
    {
        // Init var
		$tabConfig = $this->getTabConfig();
		$result = $tabConfig[ConstService::TAB_CONFIG_KEY_SOURCE];
		
		// Return result
        return $result;
    }
	
	
	
	/**
     * @inheritdoc
     * @throws ArgumentInvalidFormatException
     * @throws InstanceEnableCreateException
     */
    protected function getObjInstanceNew()
    {
        // Init var
		$result = null;
        $objDependencyCollection = $this->getObjDependencyCollection();
		$tabConfig = $this->getTabConfig();
        $strClassPath = $this->getStrClassPathEngine();
		
		// Init arguments
		$tabArg = array();
		if(isset($tabConfig[ConstService::TAB_CONFIG_KEY_ARGUMENT]))
		{
			$tabArg = $this->getTabArg($tabConfig[ConstService::TAB_CONFIG_KEY_ARGUMENT]);
		}

        // Init force access option
        $boolForceAccess = false;
		if(!is_null($objDependencyCollection))
        {
            $boolForceAccess = $objDependencyCollection->checkForceAccessRequired();
        }

		// Create instance
		$result = ToolBoxReflection::getObjInstance($strClassPath, $tabArg, $boolForceAccess);
		
		// Throw exception if creation instance failed
		if(is_null($result))
		{
			throw new InstanceEnableCreateException($strClassPath);
		}
		
		// Return result
		return $result;
    }
	
	
	
}