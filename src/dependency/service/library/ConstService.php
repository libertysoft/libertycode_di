<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\service\library;



class ConstService
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
	const TAB_CONFIG_KEY_KEY = 'key';
	const TAB_CONFIG_KEY_SOURCE = 'source';
	const TAB_CONFIG_KEY_ARGUMENT = 'argument';
	const TAB_CONFIG_KEY_ARGUMENT_TYPE = 'type';
	const TAB_CONFIG_KEY_ARGUMENT_VALUE = 'value';
	const TAB_CONFIG_KEY_ARGUMENT_KEY = 'key';

    const TAB_CONFIG_ARGUMENT_TYPE_MIXED = 'mixed';
	const TAB_CONFIG_ARGUMENT_TYPE_STRING = 'string';
	const TAB_CONFIG_ARGUMENT_TYPE_NUM = 'numeric';
	const TAB_CONFIG_ARGUMENT_TYPE_BOOL = 'boolean';
	const TAB_CONFIG_ARGUMENT_TYPE_ARRAY = 'array';
    const TAB_CONFIG_ARGUMENT_TYPE_CONFIG = 'config';
	const TAB_CONFIG_ARGUMENT_TYPE_CLASS = 'class';
	const TAB_CONFIG_ARGUMENT_TYPE_DEPENDENCY = 'dependency';
	const TAB_CONFIG_ARGUMENT_TYPE_CALLBACK_RETURN = 'callback_return';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the service configuration standard.';
    const EXCEPT_MSG_ARGUMENT_INVALID_FORMAT = 'Argument in config invalid: %1$s! The argument must follow the service configuration standard.';
    const EXCEPT_MSG_INSTANCE_ENABLE_CREATE = 'Impossible to create instance from following type "%1$s"!';



}