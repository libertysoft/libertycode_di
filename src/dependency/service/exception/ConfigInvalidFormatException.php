<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\service\exception;

use liberty_code\di\dependency\service\library\ConstService;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstService::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
			(is_array($config) && (count($config) > 0)) &&

            // Check valid string key
			isset($config[ConstService::TAB_CONFIG_KEY_KEY]) &&
            is_string($config[ConstService::TAB_CONFIG_KEY_KEY]) &&
            (trim($config[ConstService::TAB_CONFIG_KEY_KEY]) != '') &&

            // Check valid string source
			isset($config[ConstService::TAB_CONFIG_KEY_SOURCE]) &&
            is_string($config[ConstService::TAB_CONFIG_KEY_SOURCE]) &&
            (trim($config[ConstService::TAB_CONFIG_KEY_SOURCE]) != '') &&

            // Check valid array argument if found
			(
			    (!isset($config[ConstService::TAB_CONFIG_KEY_ARGUMENT])) ||
                is_array($config[ConstService::TAB_CONFIG_KEY_ARGUMENT])
            );
		
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}