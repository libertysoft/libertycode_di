<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\dependency\service\exception;

use liberty_code\di\dependency\service\library\ConstService;



class InstanceEnableCreateException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $strClassPath
     */
	public function __construct($strClassPath)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstService::EXCEPT_MSG_INSTANCE_ENABLE_CREATE,
            mb_strimwidth(strval($strClassPath), 0, 50, "...")
        );
	}
}