<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/dependency/test/InterfaceTest1.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest1.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest1_bis.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest2.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest3.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest4.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest5.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest6.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest7.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTest8.php');
require_once($strRootAppPath . '/src/dependency/test/ClassTestCall.php');

// Use
use liberty_code\data\data\table\model\TableData;
use liberty_code\register\item\instance\model\InstanceCollection;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\config\model\DefaultConfig;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\service\model\Service;
use liberty_code\di\dependency\preference\model\Preference;

// Use test
use liberty_code\di\dependency\test\ClassTest1;
use liberty_code\di\dependency\test\ClassTest8;
use liberty_code\di\dependency\test\ClassTestCall;



// Init var
$objConfig = DefaultConfig::instanceGetDefault();

$objData = new TableData();
$objConfigExt = new DataConfig($objData);
$objConfigExt->getObjData()->setDataSrc(array(
    'conf_key_1' => 'Value config 1',
    'conf_key_2' => 2.77,
    'conf_key_3' => true,
));

$objRegister = new MemoryRegister(new InstanceCollection());

$tabConfig = array(
    //'search_cache_only_require' => true,
    //'select_class_path' => 'last',
    'force_access_require' => 1,
    //'auto_config_require' => 1,
    //'max_limit_recursion' => 3
);

$objDepCollection = new DefaultDependencyCollection($objRegister, $objConfigExt, $tabConfig);



// Test configuration
/** @var DefaultConfig $objConfig */
$objConfig->setBoolAutoConfig(true);
//$objConfig->setIntMaxLimitRecursion(75); // Max 159 before fatal error



// Test add dependency
echo('Test add : <br />');

$objSvc1 = new Service(array(
    'key' => 'svc_1',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
    //*
    'argument' => [
        ['type' => 'numeric', 'value' => 1.7],
        ['type' => 'string', 'value' => 'service 1'],
        ['type' => 'boolean', 'value' => 1],
        ['type' => 'array', 'value' => [
                ['key' => 'key_1', 'type' => 'string', 'value' => 'service 1 array'],
                ['key' => 'key_2', 'type' => 'numeric', 'value' => 3],
                ['type' => 'config', 'value' => 'conf_key_3'],
                ['key' => 'key_4', 'type' => 'array', 'value' => [
                        ['key' => 'key_4_1', 'type' => 'string', 'value' => 'service 1 array array'],
                        ['key' => 'key_4_2', 'type' => 'numeric', 'value' => '7'],
                        ['key' => 'key_4_3', 'type' => 'boolean', 'value' => false],
                    ]
                ]
            ]
        ],
    ],
    //*/
    'option' => [
        'shared' => true,
        //'class_wired' => false
    ]
));

$objSvc2 = new Service(array(
    'key' => 'svc_2',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest1',
    'argument' => [
        ['type' => 'config', 'value' => 'conf_key_2'],
        ['type' => 'string', 'value' => 'service 2'],
        true //,
        //['type' => 'array', 'value' => []] // Test argument with default value
    ],
    'option' => [
        //'class_wired' => false
    ]
));

$objSvc3 = new Service(array(
    'key' => 'svc_3',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
    'argument' => [
        ['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest5'],
        ['type' => 'callback_return', 'value' => 'new \\DateTime()'],
        ['type' => 'callback_return', 'value' => function(){return 9;}] // ,
        // ['type' => 'array', 'value' => []] // Test argument with default value
    ]
));

$objSvc4 = new Service(array(
    'key' => 'svc_4',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
    'argument' => [
        ['type' => 'dependency', 'value' => 'svc_1'],
        ['type' => 'mixed', 'value' => new \DateTime()],
        ['type' => 'boolean', 'value' => '9'] //,
        //['type' => 'array', 'value' => []] // Test argument with default value
    ],
    'option' => [
        'shared' => 1
    ]
));

$objSvc5 = new Service(array(
    'key' => 'svc_5',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest4',
    'argument' => [
        ['type' => 'class', 'value' => 'liberty_code\\di\\dependency\\test\\ClassTest3'],
        //['type' => 'array', 'value' => []] // Test argument with default value
    ],
    'option' => [
        //'shared' => 1
    ]
));

$objPref1 = new Preference(array(
    'key' => 'pref_1',
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest2',
    'set' => 'liberty_code\\di\\dependency\\test\\ClassTest6',
));

$objPref2 = new Preference(array(
    'source' => 'liberty_code\\di\\dependency\\test\\ClassTest7',
    'set' =>  ['type' => 'dependency', 'value' => 'svc_4'],
));

$objPref3 = new Preference(array(
    'source' => 'liberty_code\\di\\dependency\\test\\InterfaceTest1',
    'set' =>  [
        'type' => 'callback_return',
        'value' => function() use ($objDepCollection){
            $objDep = $objDepCollection->getObjDependencyFromClassPath('liberty_code\\di\\dependency\\test\\ClassTest4');
            return $objDep->getObjInstance();
        }
    ], // callback_return
    'option' => [
        //'shared' => true,
        //'class_wired' => false
    ]
));

$objPref4 = new Preference(array(
    'source' => get_class($objDepCollection),
    'set' =>  ['type' => 'instance', 'value' => $objDepCollection],
    'option' => [
        'shared' => true,
        //'class_wired' => false
    ]
));

$objDepCollection->setDependency($objPref1);
$objDepCollection->setDependency($objPref2);
$objDepCollection->setDependency($objPref3);
$objDepCollection->setDependency($objPref4);
$objDepCollection->setDependency($objSvc1);
$objDepCollection->setDependency($objSvc2);
$objDepCollection->setDependency($objSvc3);
$objDepCollection->setDependency($objSvc4);
$objDepCollection->setDependency($objSvc5);

echo('<pre>');print_r($objDepCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from key
$tabKey = array(
	'svc_1', // Found
	'svc_2', // Found
    'svc_3', // Found
    'svc_4', // Found
	'svc_5', // Found
    'pref_1', // Found, get class 6
	'pref__liberty_code_di_dependency_test_ClassTest2', // Not found
    'pref__liberty_code_di_dependency_test_ClassTest7', // Found, get class 2
    'pref__liberty_code_di_dependency_test_InterfaceTest1', // Found, get class 4
	'key_1', // Not found
	'test', // Not found
	4, // Index found
	6, // Index found
);

foreach($tabKey as $strKey)
{
	echo('Test check, get key "'.$strKey.'": <br />');
	try{
		$objDep = $objDepCollection->getObjDependency($strKey);
		$boolDepExists = $objDepCollection->checkDependencyExists($strKey);
		
		echo('Check: <pre>');var_dump($boolDepExists);echo('</pre>');
		
		if(!is_null($objDep))
		{
			echo('Get: key: <pre>');print_r($objDep->getStrKey());echo('</pre>');
			echo('Get: class path: <pre>');print_r($objDep->getStrClassPath());echo('</pre>');
			echo('Get: config: <pre>');var_dump($objDep->getTabConfig());echo('</pre>');
			echo('Get: instance: <pre>');print_r($objDep->getObjInstance());echo('</pre>');
			echo('Get: dependency collection count: <pre>');print_r(count($objDep->getObjDependencyCollection()->getTabKey()));echo('</pre>');
		}
		else
		{
			echo('Get: not found: <br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test check/get from class path
$tabClassPath = array(
	'liberty_code\\di\\dependency\\test\\ClassTest1', // Found
	'liberty_code\\di\\dependency\\test\\ClassTest2', // Found
    'liberty_code\\di\\dependency\\test\\InterfaceTest1', // Found, get class 4
	'svc_1', // Not found
	'test_1/stdclass', // Not found
	'test', // Not found
    '\\string', // Not found
    '\\DateTime', // Not found
	4, // Not found
	6, // Not found
);

foreach($tabClassPath as $strClassPath)
{
	echo('Test check, get class path "'.$strClassPath.'": <br />');
	try{
		$objDep = $objDepCollection->getObjDependencyFromClassPath($strClassPath);
		$boolDepExists = $objDepCollection->checkDependencyExistsFromClassPath($strClassPath);
		
		echo('Check: <pre>');var_dump($boolDepExists);echo('</pre>');
		
		if(!is_null($objDep))
		{
			echo('Get: key: <pre>');print_r($objDep->getStrKey());echo('</pre>');
			echo('Get: class path: <pre>');print_r($objDep->getStrClassPath());echo('</pre>');
			echo('Get: config: <pre>');var_dump($objDep->getTabConfig());echo('</pre>');
			echo('Get: instance: <pre>');print_r($objDep->getObjInstance());echo('</pre>');
			echo('Get: dependency collection count: <pre>');print_r(count($objDep->getObjDependencyCollection()->getTabKey()));echo('</pre>');
		}
		else
		{
			echo('Get: not found! <br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test check/get from class path in autowiring mode
$tabClassPath = array(
    'liberty_code\\di\\dependency\\test\\ClassTest1_bis', // Impossible to instance
    'liberty_code\\di\\dependency\\test\\ClassTest2', // Found, get class 6
    'liberty_code\\di\\dependency\\test\\ClassTest3', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest4', // Found
	'liberty_code\\di\\dependency\\test\\ClassTest5', // Found
    'liberty_code\\di\\dependency\\test\\ClassTest7', // Found, get class 2
	'liberty_code\\di\\dependency\\test\\ClassTest8', // Found
    'liberty_code\\di\\dependency\\test\\InterfaceTest1', // Found, get class 4
    get_class($objDepCollection), // Found
    'test_1/stdclass', // Not found
    'test', // Not found
    '\\string', // Not found
    '\\DateTime', // Not found
    4, // Index found
    6 // Index not found
);

foreach($tabClassPath as $strClassPath)
{
    echo('Test check, get class path "'.$strClassPath.'" in autowiring mode: <br />');
    try{
        $objInstance = $objDepCollection->getObjInstance($strClassPath);
        $boolInstanceExists = $objDepCollection->checkInstanceExists($strClassPath);

        echo('Check: <pre>');var_dump($boolInstanceExists);echo('</pre>');

        if(!is_null($objInstance))
        {
            echo('Get: instance: <pre>');
            if($strClassPath == get_class($objDepCollection))
            {
                var_dump($objInstance);
            }
            else
            {
                print_r($objInstance);
            }
            echo('</pre>');
        }
        else
        {
            echo('Get: not found: <br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test reflection function
function testFunction(ClassTest1 $objArg1, ClassTest8 $objArg2, $arg3 = 'default value')
{
    return sprintf(
        'f[%1$s:%2$s:%3$s]',
        strval(get_class($objArg1)),
        strval(get_class($objArg2)),
        strval($arg3)
    );
}

$tabConfig = array(
    // Ko: not static
    [
        [ClassTestCall::class, 'testMethod'],
        [
            'test 1'
        ]
    ],

    // Ok
    [
        [new ClassTestCall(), 'testMethod'],
        [
            'test 1'
        ]
    ],

    // Ok
    [
        [new ClassTestCall(), 'testMethod'],
        [
            'objArg1' => new ClassTest1(7, 'callable test 1', true),
            'test 1'
        ]
    ],

    // Ko: At least argument '$arg3' require
    [
        [new ClassTestCall(), 'testMethod'],
        []
    ],

    // Ko: function not found
    [
        '_testFunction',
        ['test 2']
    ],

    // Ok
    [
        'testFunction',
        ['test 2']
    ],

    // Ok
    [
        'testFunction',
        []
    ],

    // Ok
    [
        [ClassTestCall::class, 'testStaticMethod'],
        ['arg3' => 'test 3']
    ],

    // Ok
    [
        [new ClassTestCall(), 'testStaticMethod'],
        ['test 3']
    ],

    // Ok
    [
        [new ClassTestCall(), 'testStaticMethod'],
        []
    ]
);

echo('Test callable: <br />');
foreach($tabConfig as $config)
{
    // Get info
    $configFunction = $config[0];
    $tabArg = $config[1];

    echo('Function config: <pre>');var_dump($configFunction);echo('</pre>');
    echo('Arguments: <pre>');var_dump($tabArg);echo('</pre>');

    // Call function
    try{
        $callFunctionReturn = $objDepCollection->callFunction($configFunction, $tabArg);
        echo('Callable OK:');
        echo('<pre>');var_dump($callFunctionReturn);echo('</pre>');
    } catch(\Exception $e) {
        echo('Callable KO:' . $e->getMessage() . '!');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test see register object
echo('Test register: <br />');
echo('<pre>');var_dump($objDepCollection->getObjRegister());echo('</pre>');

echo('<br /><br /><br />');


