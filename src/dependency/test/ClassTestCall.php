<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\ClassTest1;
use liberty_code\di\dependency\test\ClassTest8;



class ClassTestCall
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods test callable
    // ******************************************************************************

    public function testMethod(ClassTest1 $objArg1, ClassTest8 $objArg2, $arg3)
    {
        echo('-Arg 1: <pre>');var_dump($objArg1);echo('</pre>');
        echo('-Arg 2: <pre>');var_dump($objArg2);echo('</pre>');
        echo('-Arg 3: <pre>');var_dump($arg3);echo('</pre>');
    }



    protected static function testStaticMethod(ClassTest1 $objArg1, ClassTest8 $objArg2, $arg3 = 'default value')
    {
        echo('-Arg 1: <pre>');var_dump($objArg1);echo('</pre>');
        echo('-Arg 2: <pre>');var_dump($objArg2);echo('</pre>');
        echo('-Arg 3: <pre>');var_dump($arg3);echo('</pre>');

        return sprintf(
            's[%1$s:%2$s:%3$s]',
            strval(get_class($objArg1)),
            strval(get_class($objArg2)),
            strval($arg3)
        );
    }
}