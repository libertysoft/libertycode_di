<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\InterfaceTest1;
use liberty_code\di\dependency\test\ClassTest3;



class ClassTest4 implements InterfaceTest1
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct(ClassTest3 $objArg1, array $tabArg2 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $objArg1,
            'arg_2' => $tabArg2
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }
}