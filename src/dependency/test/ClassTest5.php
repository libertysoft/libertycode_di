<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\ClassTest1;



class ClassTest5 extends ClassTest1
{
	// ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct(ClassTest3 $objArg1 = null, array $tabArg2 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $objArg1,
            'arg_2' => $tabArg2
        );
    }
}