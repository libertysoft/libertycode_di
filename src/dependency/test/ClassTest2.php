<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\ClassTest1;



class ClassTest2
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct(ClassTest1 $objArg1, $objDtArg2, $boolArg3, array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $objArg1,
            'arg_2' => $objDtArg2,
            'arg_3' => $boolArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }
}