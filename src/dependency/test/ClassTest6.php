<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\ClassTest2;



class ClassTest6 extends ClassTest2
{
	// ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct()
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => 6,
            'arg_2' => 'class 6',
            'arg_3' => true,
            'arg_4' => array()
        );
    }
}