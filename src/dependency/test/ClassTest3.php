<?php

namespace liberty_code\di\dependency\test;

use liberty_code\di\dependency\test\ClassTest1;
use liberty_code\di\dependency\test\ClassTest2;



class ClassTest3
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /** @var array */
    protected $tabArg;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    private function __construct(ClassTest1 $objArg1, ClassTest2 $objArg2, $strArg3 = 'Class 3', array $tabArg4 = array())
    {
        // Init var
        $this->tabArg = array(
            'arg_1' => $objArg1,
            'arg_2' => $objArg2,
            'arg_3' => $strArg3,
            'arg_4' => $tabArg4
        );
    }





    // Methods getters
    // ******************************************************************************

    public function getTabArg()
    {
        // Return result
        return $this->tabArg;
    }



    public function getStrHash()
    {
        // Return result
        return spl_object_hash($this);
    }
}