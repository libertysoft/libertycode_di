<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\config\exception;

use liberty_code\di\config\library\ConstConfig;



class AutoConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $autoConfig
     */
	public function __construct($autoConfig)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_AUTO_CONFIG_INVALID_FORMAT, strval($autoConfig));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified auto-configuration has valid format
	 * 
     * @param mixed $autoConfig
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($autoConfig)
    {
		// Init var
		$result = is_bool($autoConfig);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($autoConfig);
		}
		
		// Return result
		return $result;
    }
	
	
	
}