<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\config\exception;

use liberty_code\di\config\library\ConstConfig;



class MaxLimitRecursionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $limitRecursion
     */
	public function __construct($limitRecursion)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstConfig::EXCEPT_MSG_MAX_LIMIT_RECURSION_INVALID_FORMAT, strval($limitRecursion));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified limit of recursions has valid format
	 * 
     * @param mixed $limitRecursion
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($limitRecursion)
    {
		// Init var
		$result = (is_int($limitRecursion) && ($limitRecursion >= 0));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($limitRecursion);
		}
		
		// Return result
		return $result;
    }
	
	
	
}