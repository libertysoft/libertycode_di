<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Config constants
    const DATA_KEY_DEFAULT_AUTO_CONFIG = 'boolAutoConfig';
    const DATA_KEY_DEFAULT_MAX_LIMIT_RECURSION = 'intMaxLimitRecursion';

    const DATA_DEFAULT_VALUE_AUTO_CONFIG = true;
    const DATA_DEFAULT_VALUE_MAX_LIMIT_RECURSION = 0;



    // Exception message constants
    const EXCEPT_MSG_AUTO_CONFIG_INVALID_FORMAT = 'Following auto-configuration option "%1$s" invalid! The auto-configuration option must be a boolean.';
    const EXCEPT_MSG_MAX_LIMIT_RECURSION_INVALID_FORMAT = 'Following maximum limit number of recursions "%1$s" invalid! The limit must be a positive integer.';
	


}