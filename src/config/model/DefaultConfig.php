<?php
/**
 * Description :
 * This class allows to provide configuration for default dependency tools (model and collection).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\di\config\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\di\config\library\ConstConfig;
use liberty_code\di\config\exception\AutoConfigInvalidFormatException;
use liberty_code\di\config\exception\MaxLimitRecursionInvalidFormatException;



/**
 * @method integer getIntMaxLimitRecursion() Get maximum limit of recursions for dependency search.
 * @method void setBoolAutoConfig(boolean $boolAutoConfig) Set auto-configuration option. This option allows to create a dependency instance automatically without dependency configuration.
 * @method void setIntMaxLimitRecursion(integer $intLimitRecursion) Set maximum limit of recursions for dependency search.
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_AUTO_CONFIG))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_AUTO_CONFIG, ConstConfig::DATA_DEFAULT_VALUE_AUTO_CONFIG);
		}
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_MAX_LIMIT_RECURSION))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_MAX_LIMIT_RECURSION, ConstConfig::DATA_DEFAULT_VALUE_MAX_LIMIT_RECURSION);
		}
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstConfig::DATA_KEY_DEFAULT_AUTO_CONFIG,
			ConstConfig::DATA_KEY_DEFAULT_MAX_LIMIT_RECURSION
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstConfig::DATA_KEY_DEFAULT_AUTO_CONFIG:
					AutoConfigInvalidFormatException::setCheck($value);
					break;

				case ConstConfig::DATA_KEY_DEFAULT_MAX_LIMIT_RECURSION:
					MaxLimitRecursionInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check if auto-configuration option set.
     * This option allows to create a dependency instance automatically without dependency configuration.
     *
     * @return boolean
     */
    public function checkIsAutoConfig()
    {
        // Return result
        return parent::beanGet(ConstConfig::DATA_KEY_DEFAULT_AUTO_CONFIG);
    }
	
	
	
}