<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/AutoConfigInvalidFormatException.php');
include($strRootPath . '/src/config/exception/MaxLimitRecursionInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/dependency/library/ConstDependency.php');
include($strRootPath . '/src/dependency/library/ToolBoxDependency.php');
include($strRootPath . '/src/dependency/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/dependency/exception/DependencyCollectionInvalidFormatException.php');
include($strRootPath . '/src/dependency/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/dependency/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/dependency/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/dependency/exception/MaxLimitRecursionException.php');
include($strRootPath . '/src/dependency/exception/FunctionEnableCallException.php');
include($strRootPath . '/src/dependency/api/DependencyInterface.php');
include($strRootPath . '/src/dependency/api/DependencyCollectionInterface.php');
include($strRootPath . '/src/dependency/model/DefaultDependency.php');
include($strRootPath . '/src/dependency/model/DefaultDependencyCollection.php');

include($strRootPath . '/src/dependency/service/library/ConstService.php');
include($strRootPath . '/src/dependency/service/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/dependency/service/exception/ArgumentInvalidFormatException.php');
include($strRootPath . '/src/dependency/service/exception/InstanceEnableCreateException.php');
include($strRootPath . '/src/dependency/service/model/Service.php');

include($strRootPath . '/src/dependency/preference/library/ConstPreference.php');
include($strRootPath . '/src/dependency/preference/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/dependency/preference/exception/SetInvalidFormatException.php');
include($strRootPath . '/src/dependency/preference/exception/InstanceNotFoundException.php');
include($strRootPath . '/src/dependency/preference/model/Preference.php');

include($strRootPath . '/src/dependency/factory/library/ConstDependencyFactory.php');
include($strRootPath . '/src/dependency/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/dependency/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/dependency/factory/api/DependencyFactoryInterface.php');
include($strRootPath . '/src/dependency/factory/model/DefaultDependencyFactory.php');

include($strRootPath . '/src/dependency/factory/standard/library/ConstStandardDependencyFactory.php');
include($strRootPath . '/src/dependency/factory/standard/model/StandardDependencyFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/provider/library/ConstProvider.php');
include($strRootPath . '/src/provider/exception/DependencyCollectionInvalidFormatException.php');
include($strRootPath . '/src/provider/api/ProviderInterface.php');
include($strRootPath . '/src/provider/model/DefaultProvider.php');

include($strRootPath . '/src/factory/library/ConstFactory.php');
include($strRootPath . '/src/factory/exception/ProviderInvalidFormatException.php');
include($strRootPath . '/src/factory/model/DefaultFactory.php');