LibertyCode_Di
==============



Description
-----------

Library contains dependency injection components, 
allows to manage object instantiation.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/di ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/di": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\di\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Global information

---



Usage
-----

#### Dependency

Dependencies system allows to instanciate and get object, 
from specific configuration, 
and specified dependency or class path.

_Elements_

- Dependency

    Allows to design a dependency, 
    who is an item containing configuration array, 
    to instantiate a specified class and get object.

- Service

    Extends dependency features. 
    Uses class path and constructor arguments definitions, 
    in configuration array, 
    to create instance for a specified class.

- Preference

    Extends dependency features. 
    Uses configuration, 
    in configuration array, 
    to get instance for a specified class.

- DependencyCollection

    Allows to design collection of dependencies.
    Uses list of dependencies,
    to instantiate a specified class and get object, 
    from specified dependency or string class path.

- DependencyFactory

    Allows to design a dependency factory, 
    to provide new or specified dependency instance,
    from specified configuration.
    
- StandardDependencyFactory

    Extends dependency factory features. 
    Provides dependency instance.
    
_Example_

```php
// Get dependency factory
use liberty_code\di\dependency\factory\standard\model\StandardDependencyFactory;
$dependencyFactory = new StandardDependencyFactory();
...
// Get new dependency from configuration
$dependency = $dependencyFactory->getObjDependency(array(...));
...
```

#### Builder

Builder allows to hydrate dependency collection, with routes.

_Elements_

- Builder
    
    Uses array of source data to hydrate dependency collection.

_Example_

```php
// Get dependency collection
use liberty_code\di\dependency\model\DefaultDependencyCollection;
$dependencyCollection = new DefaultDependencyCollection();
...
// Get dependency builder
use liberty_code\di\build\model\DefaultBuilder;
$dependencyBuilder = new DefaultBuilder($dependencyFactory);
...
// Hydrate dependency collection
$dependencyBuilder->setTabDataSrc(array(...));
$dependencyBuilder->hydrateDependencyCollection($dependencyCollection);
...
foreach($dependencyCollection->getTabKey() as $key) {
    echo($dependencyCollection->getObjDependency($key)->getStrKey() .'<br />');
}
/**
 * Show: 
 * Dependency key 1
 * ...
 * Dependency key N
 */
...
```

#### Provider

Provider allows to instantiate and get configured instances, 
using dependency collection.

_Example_

```php
use liberty_code\di\provider\model\DefaultProvider;
$provider = new DefaultProvider($dependencyCollection);
...
// Show specified instance
var_dump($provider->get(...));
...
```

#### Factory

Factory allows to instantiate and get new instances, 
using provider.

_Elements_

- Factory

    Allows to design a factory, 
    to provide new instance, 
    from specified DI provider.
    Can be consider is base of all factory types.
    
---


